import re

class TextToEmotag:

    """
    Classe che contiene delle metodi che fungono da utilities per passare dalla riga di testo
    ai tipi di emoji contenuti in essa.
    """

    emodic = {':-)': '#emo0#',
               ':)': '#emo1#',
               ':(': '#emo2#',
               ':\'(': '#emo3#',
               ':\')': '#emo4#',
               ':D': '#emo5#',
               'D:': '#emo6#',
               ':O': '#emo7#',
               ':o': '#emo8#',
               'o_o': '#emo9#',
               'O_O': '#emo10#',
               ';)': '#emo11#',
               ';-)': '#emo12#',
               'XD': '#emo13#',
               'xD': '#emo14#',
               ':P': '#emo15#',
               ':p': '#emo16#',
               '^_^': '#emo17#',
               '^-^': '#emo18#',
               ':|': '#emo19#',
               '<3': '#emo20#',
               '♥': '#emo21#',
               '❤': '#emo22#',
               ':\\': '#emo23#',
               ':c': '#emo24#',
               ':3': '#emo25#',
               ':*': '#emo26#',
               'xd': '#emo27#'}

    @staticmethod
    def convert(text):

        """
        Restituisce una stringa contenente i gli "emotag" contenuti nella riga originale.
        Esempio: "brava!:) io ero rimasta ai tempi del progetto positività.. come sta andando? sembra bene:)"
        Stringa restutiota: "#emo1# #emo1# "

        :param text: stringa di cui si vogliono gli emotag
        :return: stringa contenente gli emotag
        """

        newline = ""

        for emo in TextToEmotag.emodic.keys():
            pat = re.compile(re.escape(emo))
            matches = re.findall(pat, text)
            for match in matches:
                newline += TextToEmotag.emodic[emo] + " "

        # le secuenti righe servono a gestire i caratteri non BMP qualora questi ultimi siano stati appropriatamente
        # sostituiti nel testo con i rispettivi codici esadecimali
        pat = re.compile(r'(0x.{5}\b)')
        matches = re.findall(pat, text)

        for match in matches:
            newline += '#emo' + match + '# '

        # le seguenti righe servono invece a gestire alcune altre tipiche emoticon che conviene gestire tramite regex
        # e non tramite semplice match con le entries del dizionario

        # O____O o_O O-O ecc.
        pat = re.compile(r'[oO][-_]+[oO]')
        matches = re.findall(pat, text)

        for match in matches:
            newline += '#emoO# '

        # *_*, *----*, ecc.
        pat = re.compile(r'\*[-_]+\*')
        matches = re.findall(pat, text)

        for match in matches:
            newline += '#emoA# '

        # U___U u_u u-u, ecc.
        pat = re.compile(r'[uU][-_]+[uU]')
        matches = re.findall(pat, text)

        for match in matches:
            newline += '#emoU# '

        # *o* *O* *oooo*
        pat = re.compile(r'\*[oO]+\*')
        matches = re.findall(pat, text)

        for match in matches:
            newline += '#emoOFACE# '

        # loooool, Looool, LOOOOOL
        pat = re.compile(r'[lL][oO]+[lL]')
        matches = re.findall(pat, text)

        for match in matches:
            newline += '#emoLOL# '

        # çç ç___ç
        pat = re.compile(r'ç[-_]*ç')
        matches = re.findall(pat, text)

        for match in matches:
            newline += '#emoç# '

        # uwu
        pat = re.compile(r'[uùU][wW]+[uùU]')
        matches = re.findall(pat, text)

        for match in matches:
            newline += '#emow# '

        if len(newline) == 0:
            newline += "#empty#"

        return newline
