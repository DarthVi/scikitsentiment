import treetaggerwrapper
import nltk

class Tokenizer:

    """
    Custom Tokenizer. Permette di ottenere lemmi o stemmi di una frase, scegliendo se mantenere o meno le stopwords
    """

    # custom translator usato per eliminare i segni di punteggiatura
    translator = str.maketrans({key: None for key in ',.;'})
    remchar = ['\'', '\\', '/', ':', ';', ',', '.']

    def __init__(self, stopwords=None, stemmer=None):
        """
        Usa una lista di parole come stopwords da eliminare o se è None (default) mantiene le stopwords.
        Se stemmer è lasciato con il valore di default (None) verranno ottenuti i lemmi.
        Viceversa, se si passa uno stemmer che implementa il metodo stem(string), verranno ottenuti gli stem

        :param stopwords: lista di stringhe
        :param stemmer: stemmer (deve implementare il metodo stem)
        """
        self.treetagger = treetaggerwrapper.TreeTagger(TAGLANG='it', TAGPARFILE='italian.par')
        self.stopwords = stopwords
        self.stemmer = stemmer

    def get_tokens(self, text):

        """
        Restituisce lemmi o stemmi (a seconda di quanto fornito al costruttore) contenuti nella stringa data
        in input
        :param text: stringa
        :return: lista di token (stemmi o lemmi) contenuti nella stringa di input
        """

        # eliminazione dei segni di punteggiatura
        #no_punctuation = text.translate(self.translator)


        tknz = nltk.tokenize.TweetTokenizer()
        tokens = tknz.tokenize(text)
        if self.stopwords is not None:
            filtered = [w for w in tokens if not w in self.stopwords]
        else:
            filtered = tokens
        no_punctuation = ' '.join(filtered)

        tags = self.treetagger.tag_text(no_punctuation)
        list = [s.split('\t') for s in tags]

        if self.stemmer is not None:
            listconcat = [self.stemmer.stem(s[0].lower()) if s[1] != 'NPR' else self.stemmer.stem(s[0]) for s in list
                          if len(s) == 3 and s[0] not in Tokenizer.remchar]
        else:
            listconcat = [s[2].lower() if s[1] != 'NPR' else s[2] for s in list
                          if len(s) == 3 and s[0] not in Tokenizer.remchar]

        return listconcat

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['treetagger']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self.treetagger = treetaggerwrapper.TreeTagger(TAGLANG='it', TAGPARFILE='italian.par')