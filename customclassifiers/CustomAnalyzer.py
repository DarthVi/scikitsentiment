from customclassifiers.CustomTokenizer import Tokenizer

class TweetAnalyzer:

    """
    Custom analyzer da usare in CountVectorizer
    """

    def __init__(self, stopwords=None, stemmer=None):
        self.tokenizer = Tokenizer(stopwords, stemmer)

    def __call__(self, doc):
        return self.tokenizer.get_tokens(doc)

    def analyze(self, doc):
        return self.tokenizer.get_tokens(doc)

    def __str__(self):

        stopwords = "stopwords_list" if self.tokenizer.stopwords is not None else "None"
        stemmer = self.tokenizer.stemmer if self.tokenizer.stemmer is not None else "None"

        return "TwitterAnalyzer(" + stopwords.__str__() + "," + stemmer.__str__() + ")"

    def __repr__(self):

        stopwords = "stopwords_list" if self.tokenizer.stopwords is not None else "None"
        stemmer = self.tokenizer.stemmer if self.tokenizer.stemmer is not None else "None"

        return "TwitterAnalyzer(" + stopwords.__repr__() + "," + stemmer.__repr__() + ")"