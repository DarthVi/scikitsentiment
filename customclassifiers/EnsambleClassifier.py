from sklearn.base import BaseEstimator
from sklearn.base import ClassifierMixin
from sklearn.preprocessing import LabelEncoder
import numpy as np
import operator

class EnsembleClassifier(BaseEstimator, ClassifierMixin):
    """
    Accorpa i classificatori e usa questi ultimi con cercando di ottenere le classificazioni
    tramite il majority vote o usando i pesi e le probabilità di appartenenza alle classi.
    """
    def __init__(self, clfs, weights=None, fallback_label=None):
        self.clfs = clfs
        self.weights = weights
        self.fallback_label = fallback_label

    def fit(self, X, y):
        """
        Chiama il metodo fit di entrambi i classificatori
        :param X: train data
        :param y: train lables
        :return:
        """

        if self.weights is not None and len(self.weights) != len(self.clfs):
            raise ValueError("La lista dei pesi, se presente, deve presentare un numero di pesi pari al numero di classificatori")


        self.le_ = LabelEncoder()
        self.le_.fit(y)
        self.classes_lables_ = self.le_.classes_

        if self.fallback_label is not None:
            if type(self.fallback_label) is not list or all(type(self.fallback_label[i]) != list for
                                                            i in range(len(self.fallback_label))):
                raise TypeError("Il parametro fallback_label deve essere None o una lista di tipo [[i, value], "
                                "[i+1, value2] dove "
                                "i è l'indice del classificatore a cui si vuole assegnare la label value per valutare "
                                "i pareggi durante l'hard-voting")
            for l in self.fallback_label:
                self.clfs[l[0]].set_fallbacklabel(np.where(self.le_.classes_ == l[1])[0][0])

        for clf in self.clfs:
            clf.fit(X, self.le_.transform(y))

        return self

    def predict(self, X):
        """
        Classifica gli elementi presenti in X. Adotta dei meccanismi di fallback qualora si decida di utilizzare dei
        dei pesi e non il majority vote e non è presente il metodo predict_proba per tutti i classificatori.
        :param X: elementi da classificare
        :return: lista contenente le classificazioni
        """
        import numbers

        self.classes_ = np.asarray([clf.predict(X) for clf in self.clfs]).T

        # "soft"-voting tramite pesi da associare alle probabilità delle classi
        if self.weights is not None:

            for weight in self.weights:
                if not isinstance(weight, numbers.Number):
                    raise TypeError("I pesi devono essere dei valori numerici")

            # se un particolare clf non ha il metodo per ottenere le probabilità di appartenenza di un n_sample alle
            # varie n_classes, viene resettato a 1 il suo rispettivo peso perché nel meccanismo di fallback verrà
            # settato ad 1.0 la probabilità che l'item analizzato appartenga alla classe restituita dal metodo predict
            # for weight, clf in zip(self.weights, self.clfs):
            #     if not callable(getattr(clf, 'predict_proba', None)):
            #         index = self.weights.index(weight)
            #         self.weights[index] = 1

            avg = self.predict_proba(X)

            maj = np.apply_along_axis(lambda x: max(enumerate(x), key=operator.itemgetter(1))[0], axis=1, arr=avg)

        else:
            maj = np.apply_along_axis(lambda x: np.argmax(np.bincount(x, weights=None)), axis=1, arr=self.classes_)

        maj = self.le_.inverse_transform(maj)

        return maj

    def predict_proba(self, X):

        self.probas_ = [clf.predict_proba(X) if callable(getattr(clf, 'predict_proba', None)) else
                        self.build_fallback_predictproba(clf, X) for clf in self.clfs]

        avg = np.average(self.probas_, axis=0, weights=self.weights)

        return avg

    def build_fallback_predictproba(self, classifier, X):
        """
        Costruisce un callable di riserva per i classificatori a cui manca il metodo predict_proba.
        Le probabilità sono tutte settate a 0 tranne quella della classe restituita dal metodo predict.

        :param classifier: classificatore
        :param X: dati di cui ottenere le probabilità
        :return: probas, array [n_samples] x [n_classes], contenente per ogni n_samples i valori di probabilità
                 delle n_classes
        """
        classes = classifier.classes_
        #probas = np.array([[np.float64(0) for i in range(len(classes))]])
        probas = list()

        for entry in X:
            prd = classifier.predict([entry])
            prediction = prd[0]
            #predindex = classes.index(prediction)
            predindex = np.where(classes==prediction)[0][0]
            #print(predindex)
            #print(probas)
            temparray = np.array([np.float64(0), np.float64(0)])
            temparray[predindex] = np.float64(1.0)
            probas.append(temparray)

        return np.array(probas)