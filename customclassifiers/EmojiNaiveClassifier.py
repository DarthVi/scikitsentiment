from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
import numpy as np
import re

class Emodict:

    def __init__(self, emoclasses=None):

        """
        Inizializza i dati necessari all'oggetto, come il dizionario delle emoji

        :param emoclasses: lista di classi a cui associare le emoji quando vengono analizzate le righe e chiamata
                           la funzione di update. Se viene lasciato il parametro di default (None), verrà considerata
                           la lista ['positive', 'negative']
        """

        if not isinstance(emoclasses, list) and not isinstance(emoclasses, np.ndarray):
            raise ValueError("classes deve essere una lista o un numpy.ndarray")

        self.emojis = dict()
        emolist = [':-)', ':)', ':(', ':\'(', ':\')', ':D', 'D:', ':O', ':o', 'o_o', 'O_O', ';)',
                   ';-)', 'XD', 'xD', ':P', ':p', '^_^', '^-^', ':|', '<3', '♥', '❤', ':\\', '*-*',
                   '*--*', '*_*', '*__*']

        for emo in emolist:
            self.emojis[emo] = {label: 0 for label in emoclasses}

    def add_emoji(self, emo, label, count=0):
        """
        Aggiunge l'emoji emo e setta il counter dell'associazione fra emo e label a count
        :param emo: emoticon da aggiunger
        :param count: numero di occorrenze per la classe lable
        :param label: classe associata all'emoticon
        :return:
        """
        self.emojis[emo][label] = count

    def update_count(self, line, line_class):

        """
        Aggiorna i contatori del dizionario di emoticon e delle rispettive label analizzando la riga catalogata con una
        specifica label.
        Gestisce anche i caratteri non BMP
        :param line: riga da analizzare
        :param line_class: label di classificazione della riga
        """

        for emo in self.emojis.keys():
            pat = re.compile(re.escape(emo))
            matches = re.findall(pat, line)
            if line_class not in self.emojis[emo].keys():
                self.emojis[emo][line_class] = len(matches)
            else:
                self.emojis[emo][line_class] += len(matches)

        # le secuenti righe servono a gestire i caratteri non BMP qualora questi ultimi siano stati appropriatamente
        # sostituiti nel testo con i rispettivi codici esadecimali
        pat = re.compile(r'0x.{5}\b')
        matches = re.findall(pat, line)

        for match in matches:
            if match not in self.emojis.keys():
                self.emojis[match] = {}
            counts = matches.count(match)
            if line_class not in self.emojis[match].keys():
                self.emojis[match][line_class] = counts
            else:
                self.emojis[match][line_class] += counts

    def predict_weights(self, line):

        """
        Ritorna la classificazione della riga
        :param line: riga da classificare
        :return: label di classificazione
        """

        prediction = dict()
        templist = list(self.emojis)
        innertemplist = list(self.emojis[templist[0]])

        for i, item in enumerate(innertemplist):
            prediction[innertemplist[i]] = 0

        # per ogni emoji nel catalogo di emoji, ne controlla l'occorrenza nel testo;
        # se viene trovata un'occorrenza, incrementa i contatori delle rispettive classi
        # con i valori contenuti nel catalogo e calcolati durante l'addestramento
        for emo in self.emojis.keys():
            pat = re.compile(re.escape(emo))
            match = re.search(pat, line)
            if match is not None:
                for key in prediction.keys():
                    prediction[key] += self.emojis[emo][key]

        return prediction




class EmojiClassifier(BaseEstimator, ClassifierMixin):
    """Classificatore usato per fare apprendimento sulle emoticon; lavora su due classi"""

    def __init__(self):
        self.fallback_label_ = None

    def fit(self, X, y):

        self.le_ = LabelEncoder()
        self.le_.fit(y)
        self.classes_ = self.le_.classes_
        self.emodict_ = Emodict(self.classes_)

        if len(X) != len(y):
            raise ValueError("X deve avere lo stesso numero di elementi di y")

        for text, label in zip(X, y):
            if type(text) is not str:
                raise TypeError("Questo classificatore funziona solo su dati testuali")
            self.emodict_.update_count(text, label)

        return self


    def set_fallbacklabel(self, fallbacklabel):
        """
        Setta la label con cui classificare i casi di pareggio fra label nel metodo predict()
        :param fallbacklabel:   label appartenente alle classi di classificazione
        """
        self.fallback_label_ = fallbacklabel

    def predict(self, X):
        """
        Ritorna le classificazioni
        :param X: lista contenente i testi da classificare
        :return: lista contenente le classificazioni dei rispettivi testi
        """

        try:
            getattr(self, "classes_")
        except AttributeError:
            raise RuntimeError("Devi prima addestrare il classificatore!")

        import operator

        classification = list()

        for text in X:
            if type(text) is not str:
                raise TypeError("Questo classificatore lavora solo su stringhe!")
            prediction_weights = self.emodict_.predict_weights(text)
            #se è stata esplicitata una fallback_label, la usa in caso di pareggio fra i pesi delle classi
            if self.fallback_label_ is not None and len(set(prediction_weights.values())) == 1:
                predicted_label = self.fallback_label_
            else:
                predicted_label = max(iter(prediction_weights.items()), key=operator.itemgetter(1))[0]
            classification.append(predicted_label)

        return np.array(classification)


    def predict_proba(self, X):
        """
        Ritorna una lista contenente le probabilità che il testo appartenga alle classi.
        Usare zip(clf.classes_, clf.predictProba(X)) per capire a quali classi appartengono le probabilità
        :param X: lista di testi di cui si vogliono calcolare le probabilità di appartenenza alle varie classi
        :return: probabilità di appartenenza alle varie classi
        """
        try:
            getattr(self, "classes_")
        except AttributeError:
            raise RuntimeError("Devi prima addestrare il classificatore!")

        probas = list()

        for text in X:
            if type(text) is not str:
                raise TypeError("Questo classificatore lavora solo su stringhe!")
            prediction_weights = self.emodict_.predict_weights(text)

            denominator = 0
            for label in self.classes_:
                denominator += prediction_weights[label]

            if denominator == 0:
                denominator = 1

            probas.append([np.float64(prediction_weights[label]/denominator) for label in self.classes_])

        return np.array(probas, dtype=np.float64)




    def score(self, X, y, **kwargs):
        """
        Ritorna l'accuracy
        :param **kwargs:
        :param X:
        :param y:
        :return:
        """

        return accuracy_score(y, self.predict(X))
