from gensim import utils
from gensim.models.doc2vec import TaggedDocument
from gensim.models import Doc2Vec
import numpy as np
from sklearn.base import TransformerMixin
from sklearn.preprocessing import LabelEncoder
from random import shuffle
from customclassifiers.CustomTokenizer import Tokenizer
import nltk
import multiprocessing
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn import svm

cores = multiprocessing.cpu_count()

class Doc2VecModel:

    """
    Crea un modello Doc2Vec (sfruttando gensim) a partire dai documenti (frasi) forniti nel metodo fit().
    I parametri usati per l'intera procedura sono definiti nel metodo __init__ e quindi passati tramite il costruttore.
    """

    def __init__(self, min_count=1, window=5, size=140, sample=1e-4, negative=5, workers=cores, epoch=5,
                 alpha=0.025, min_alpha=0.025, dm=1):

        """
        I parametri da usare per il costruttore di Doc2Vec e per il learning

        :param min_count:
        :param window:
        :param size:
        :param sample:
        :param negative:
        :param workers:
        :param epoch:
        """
        self.min_count = min_count
        self.window = window
        self.size = size
        self.sample = sample
        self.negative = negative
        self.workers = workers
        self.epoch = epoch
        self.alpha = alpha
        self.min_alpha = min_alpha
        self.dm = dm

    def fit(self, X):

        """
        Crea un modello Doc2Vec usando i parametri definiti nel costruttore e apprendendo il vocabolario
        dagli elementi contenuti in X.
        :param X:
        :return:
        """

        trainlength = len(X)

        self.sentences_ = list()

        tokenizer = Tokenizer()

        for i, text in enumerate(X):
            self.sentences_.append(TaggedDocument(tokenizer.get_tokens(text), ['SENT' + '_%s' % i]))

        self.model_ = Doc2Vec(min_count=self.min_count, window=self.window,
                              size=self.size, sample=self.sample,negative=self.negative, workers=self.workers)
        self.model_.build_vocab(self.sentences_)

        # per migliorare il modello
        for epoch in range(self.epoch):
            self.model_.train(self.sentences_)
            self.model_.alpha -= 0.002  # decrease the learning rate
            self.model_.min_alpha = self.model_.alpha  # fix the learning rate, no decay
            shuffle(self.sentences_)

        newX = np.asarray([self.model_.docvecs['SENT' + '_%s' % i] for i in range(trainlength)])

        return newX

    def save_model(self, path='./saved_model.d2v'):
        self.model_.save(path)

    def load_model(self, path='./saved_model.d2v'):
        self.model_ = Doc2Vec.load(path)

    def get_model(self):
        """
        Ottiene il modello a patto che esso sia stato creato e addestrato. In caso contrario solleva
        un'eccezione
        :return: self.model_    modello Doc2Vec
        """
        try:
            getattr(self, "model_")
        except AttributeError:
            raise RuntimeError("Devi prima addestrare il modello Doc2vec o caricarlo dal file")

        return self.model_


class GensimClassifierWrapper(BaseEstimator, ClassifierMixin):

    def __init__(self, min_count=1, window=5, size=140, sample=1e-4, negative=5, workers=cores, epoch=20,
                 alpha=0.025, min_alpha=0.025, dm=1, classifier=None):

        """
        I parametri da usare per il costruttore di Doc2Vec e per il learning

        :param min_count:
        :param window:
        :param size:
        :param sample:
        :param negative:
        :param workers:
        :param epoch:
        """
        self.min_count = min_count
        self.window = window
        self.size = size
        self.sample = sample
        self.negative = negative
        self.workers = workers
        self.epoch = epoch
        self.alpha = alpha
        self.min_alpha = min_alpha
        self.dm = dm

        if classifier is None:
            self.classifier = svm.LinearSVC()
        else:
            self.classifier = classifier

    def fit(self, X, y):

        """
        Costruisce il Doc2Vec model, ne fa il il training usando la classe apposita (Doc2vecModel) ed ottiene
        i numpy array di doc2vec. Usa questi ultimi come input del metodo fit del classificatore, affiancati alle label.
        :param X: lista del training set
        :param y: label del dest set
        :return:
        """
        self.d2v_ = Doc2VecModel(min_count=self.min_count, window=self.window,
                              size=self.size, sample=self.sample,negative=self.negative, workers=self.workers)

        fittedData = self.d2v_.fit(X)
        self.classifier.fit(fittedData, y)

        self.transformer_ = Doc2VecTransformer(self.d2v_.get_model())
        return self

    def predict(self, X):

        """
        Usa il Doc2VecTransformer per ottenere, tramite infer_vector, il numpy array di ogni entry di X inferito sulla
        base dei dati di training. Utilizza questi numpy array inferiti come input per il metodo predict del
        classificatore wrappato.
        :param X: lista di tweet da classificare
        :return: lista di label classificati
        """

        try:
            getattr(self, "d2v_")
        except AttributeError:
            raise RuntimeError("Devi prima addestrare il classificatore!")

        return self.classifier.predict(self.transformer_.transform(X))

    def predict_proba(self, X):
        if callable(getattr(self.classifier, 'predict_proba', None)):
            return self.classifier.predict_proba(self.transformer_.transform(X))
        else:
            raise AttributeError("Embedded classifier has no attribute predict_proba")

    def score(self, X, y, **kwargs):
        """
        Chiama la funzione di scoring
        :param X:
        :param y:
        :param kwargs:
        :return:
        """

        return self.classifier.score(X, y, **kwargs)


class Doc2VecTransformer(TransformerMixin):

    """
    Transformer custom che usa un modello gensim già addestrato per inferire i numpy array da usare per il metodo
    predict.
    """

    def __init__(self, model):
        """
        :param model:   modello Doc2Vec precedentemente addestrato
        """
        self.model = model

    def transform(self, X):
        """
        Ottiene i numpy array degli elementi contenuti in X, cercando di inferirne i valori dal modello
        Doc2Vec fornito nel costruttore
        :param X:   lista di frasi (stringhe) di cui ottenere il la rappresentazione tramite numpy array
        :return:    array di numpy array
        """

        tokenizer = Tokenizer()

        retvec = list()
        for text in X:
            # mostSimilarVecKey = self.model.docvecs.most_similar(positive=
            #                                               [self.model.infer_vector(tokenizer.get_tokens(text))])[0][0]
            # retvec.append(self.model.docvecs[mostSimilarVecKey])
            retvec.append(self.model.infer_vector(tokenizer.get_tokens(text)))

        return np.asarray(retvec)