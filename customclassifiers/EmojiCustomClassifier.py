from customclassifiers.EmojiPreprocessing import TextToEmotag
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.base import BaseEstimator, ClassifierMixin


class EmojiCustomClassifier(BaseEstimator, ClassifierMixin):

    """
    Wrappa un classificatore (da dare come parametro al costruttore) consentendone l'uso per la classificazione
    dei testi basata sulla presenza di emoticon. Tale classificatore viene posto in pipeline preceduto dal
    preprocessing del testo con la classe TextToEmotag, dal CountVectorizer e dal TfidfTransformer
    """

    def __init__(self, clf=None, use_tfidf=True, analyzer='word'):

        """
        Qualora clf sia None (opzione di default) verrà scelto il Multinomial Naive Bayes classifier
        :param clf:
        """

        if clf is None:
            self.clf = MultinomialNB()
        else:
            self.clf = clf

        self.use_tfidf = use_tfidf
        self.analyzer = analyzer

        if self.use_tfidf is True:
            self.pipeline_ = Pipeline([('vect', CountVectorizer(analyzer=self.analyzer)),
                                ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                                ('clf', self.clf)])
        else:
            self.pipeline_ = Pipeline([('vect', CountVectorizer(analyzer=self.analyzer)),
                                       ('clf', self.clf)])

    def fit(self, X, y):

        """
        Effettua il training del classificatore
        :param X: training test
        :param y: label del training test con le annotazioni delle classi a cui appartiene ogni entry di X
        :return: self
        """

        newX = [TextToEmotag.convert(line) for line in X]

        self.pipeline_.fit(newX, y)
        self.classes_ = self.clf.classes_

        return self

    def predict(self, X):
        """
        Classifica gli elementi in X
        :param X: elementi da classificare
        :return: lista di classificazioni degli elementi in X; ogni X[i] avrà una i-esima label nella lista di ritorno
        """

        try:
            getattr(self, "classes_")
        except AttributeError:
            raise RuntimeError("Devi prima addestrare il classificatore!")

        newX = [TextToEmotag.convert(line) for line in X]

        return self.pipeline_.predict(newX)

    def predict_proba(self, X):
        if callable(getattr(self.pipeline_, 'predict_proba', None)):
            newX = [TextToEmotag.convert(line) for line in X]
            return self.pipeline_.predict_proba(newX)
        else:
            raise AttributeError("Embedded classifier has no attribute predict_proba")

    def score(self, X, y, **kwargs):
        """
        Chiama la funzione di scoring
        :param X:
        :param y:
        :param kwargs:
        :return:
        """

        return self.pipeline_.score(X, y, **kwargs)

    def get_use_tfidf(self):

        return self.use_tfidf