from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.ensemble import VotingClassifier
from sklearn.cross_validation import KFold
from sklearn.metrics import f1_score
from customclassifiers.EmojiNaiveClassifier import EmojiClassifier
from customclassifiers.EmojiCustomClassifier import EmojiCustomClassifier
from customclassifiers.CustomAnalyzer import TweetAnalyzer
from nltk.corpus import stopwords
import GetData16
from EvalFileBuilder import Builder
import nltk
from sklearn import svm
from time import time
from customclassifiers.Doc2VecUtils import Doc2VecModel, Doc2VecTransformer
from customclassifiers.CustomAnalyzer import TweetAnalyzer
import numpy as np

if __name__ == "__main__":

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    test_id = []

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)
    spwords = ['è', 'e', 'il', 'di', 'a', 'da', 'dello', 'del', 'un']

    t0 = time()
    dataPos, labelPos, idPos = GetData16.getPosVsWorld("training_set_sentipolc16.csv")

    dataPos = np.asarray(dataPos)
    labelPos = np.asarray(labelPos)

    text_clfPos = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(stemmer=stemmer),
                                                     max_df=0.5, max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=False, max_iter=1000)),
                            ])

    emo_clfPos = EmojiCustomClassifier()

    kfPos = KFold(n=len(dataPos), n_folds=10, shuffle=True)

    votingclf_Pos = VotingClassifier(estimators=[('text_clfPos', text_clfPos), ('emo_clfPos', emo_clfPos)],
                                     voting='hard')

    i = 1
    f_score = 0.0
    for train_index, test_index in kfPos:
        X_train, X_test = dataPos[train_index], dataPos[test_index]
        y_train, y_test = labelPos[train_index], labelPos[test_index]

        votingclf_Pos.fit(X_train, y_train)
        predictionPos = votingclf_Pos.predict(X_test)

        print("VotingClassifier SVC_EmojiClassifier_PosVsWorld Fold i: ", i)
        print(classification_report(y_test, predictionPos))
        print("Classification accuracy: ", votingclf_Pos.score(X_test, y_test))
        temp_f_measure = f1_score(y_test, predictionPos, pos_label=None, average='macro')
        f_score += temp_f_measure
        print("Classification f1-score: ", temp_f_measure)
        i += 1

    print("\n\n")

    print("Positive vs World f1-score macro: ", f_score / (i - 1))

    print("\n\n")

    dataNeg, labelNeg, idNeg = GetData16.getNegVsWorld("training_set_sentipolc16.csv")

    dataNeg = np.asarray(dataNeg)
    labelNeg = np.asarray(labelNeg)

    text_clfNeg = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(stemmer=stemmer),
                                                     max_df=0.5, max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=True, max_iter=1000)),
                            ])

    emo_clfNeg = EmojiCustomClassifier()


    votingclf_Neg = VotingClassifier(estimators=[('text_clfNeg', text_clfNeg), ('emo_clfNeg', emo_clfNeg)], voting='hard')

    kfNeg = KFold(n=len(dataNeg), n_folds=10, shuffle=True)

    i = 1
    f_score = 0.0
    for train_index, test_index in kfNeg:
        X_train, X_test = dataNeg[train_index], dataNeg[test_index]
        y_train, y_test = labelNeg[train_index], labelNeg[test_index]

        votingclf_Neg.fit(X_train, y_train)
        predictionNeg = votingclf_Neg.predict(X_test)

        print("VotingClassifier SVC_EmojiClassifier_NegVsWorld Fold: ", i)
        print(classification_report(y_test, predictionNeg))
        print("Classification accuracy: ", votingclf_Neg.score(X_test, y_test))
        temp_f_measure = f1_score(y_test, predictionNeg, pos_label=None, average='macro')
        f_score += temp_f_measure
        print("Classification f1-score: ", temp_f_measure)
        i += 1

    print("\n\n")

    print("Negative vs World f1-score macro: ", f_score / (i - 1))

    print("\n\n")

    print("done in %0.3fs" % (time() - t0))