from TweetAnalyzer import TweetAnalyzer
import TweetPurgeLib as tpl
import csv
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.linear_model import SGDClassifier
import GetData14
from EvalFileBuilder import Builder
import nltk
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV
from sklearn import svm

if __name__ == "__main__":

    classes = ['positive', 'rest_world']

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    test_id = []

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)

    train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld("sentipolc annotation gold v2.csv")

    # text_clfPos = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(None, None))),
    #                      ('tfidf', TfidfTransformer(use_idf=True)),
    #                      ('clf', SGDClassifier(loss='hinge', penalty='l2',
    #                                            alpha=1e-3, n_iter=5, random_state=None)),
    #                      ])

    text_clfPos = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(None, stemmer))),
                            ('tfidf', TfidfTransformer(use_idf=True)),
                            ('clf', svm.LinearSVC(dual=False, class_weight={'positive': 4, 'rest_world': 1})),
                            ])

    parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
                  'tfidf__use_idf': (True, False),
                  'clf__alpha': (1e-2, 1e-3),
                  }

    text_clfPos.fit(train_data, train_labels)
    predictionPos = text_clfPos.predict(test_data)

    print("SGDClassifier_PosVsWorld")
    print(classification_report(test_labels, predictionPos))

    train_dataNeg, train_labelsNeg, test_dataNeg, test_labelsNeg, test_idNeg = GetData14.getNegVsWorld("sentipolc annotation gold v2.csv")

    # text_clfNeg = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(None, None))),
    #                      ('tfidf', TfidfTransformer(use_idf=True)),
    #                      ('clf', SGDClassifier(loss='hinge', penalty='l2',
    #                                            alpha=1e-3, n_iter=5, random_state=None)),
    #                      ])

    text_clfNeg = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(None, stemmer))),
                            ('tfidf', TfidfTransformer(use_idf=True)),
                            ('clf', svm.LinearSVC(dual=False, class_weight='balanced')),
                            ])

    text_clfNeg.fit(train_dataNeg, train_labelsNeg)
    predictionNeg = text_clfNeg.predict(test_dataNeg)

    print("SGDClassifier_NegVsWorld")
    print(classification_report(test_labelsNeg, predictionNeg))

    evalbuilder = Builder("overallRun.csv", "exclude_tweets.txt")

    evalbuilder.getResFile(test_id, predictionPos, test_idNeg, predictionNeg)