from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from customclassifiers.EnsambleClassifier import EnsembleClassifier as VotingClassifier
from customclassifiers.EmojiNaiveClassifier import EmojiClassifier
from customclassifiers.CustomAnalyzer import TweetAnalyzer
from customclassifiers.EmojiCustomClassifier import EmojiCustomClassifier
from nltk.corpus import stopwords
import GetData14
from EvalFileBuilder import Builder
import nltk
from sklearn import svm
from time import time

if __name__ == "__main__":

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    test_id = []

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)

    t0 = time()
    train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld("sentipolc annotation gold v2.csv")

    text_clfPos = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(stemmer=stemmer), max_df=0.5, max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=False, max_iter=1000)),
                            ])
    emo_clfPos = EmojiCustomClassifier()

    votingclf_Pos = VotingClassifier([text_clfPos, emo_clfPos], weights=[0.8, 1.5])

    votingclf_Pos.fit(train_data, train_labels)
    predictionPos = votingclf_Pos.predict(test_data)

    print("VotingClassifier SVC_EmojiClassifier_PosVsWorld")
    print(classification_report(test_labels, predictionPos))

    train_dataNeg, train_labelsNeg, test_dataNeg, test_labelsNeg, test_idNeg = GetData14.getNegVsWorld("sentipolc annotation gold v2.csv")

    text_clfNeg = Pipeline([('vect', CountVectorizer('word', max_df=0.5, max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=True, max_iter=1000)),
                            ])

    emo_clfNeg = EmojiCustomClassifier()
    votingclf_Neg = VotingClassifier([text_clfNeg, emo_clfNeg])
    votingclf_Neg.fit(train_dataNeg, train_labelsNeg)
    predictionNeg = votingclf_Neg.predict(test_dataNeg)

    print("VotingClassifier SVC_EmojiClassifier_NegVsWorld")
    print(classification_report(test_labelsNeg, predictionNeg))
    print("done in %0.3fs" % (time() - t0))

    evalbuilder = Builder("overallRun.csv")

    evalbuilder.getResFile(test_id, predictionPos, test_idNeg, predictionNeg)