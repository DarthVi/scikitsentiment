import csv
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
import GetData14
from SWvsSciKitBuilder import Builder
import nltk
from sklearn import svm

def getListToTest(path):

    testlist = list()

    with open(path, "r", encoding="utf-8") as file:
        reader = csv.reader(file)

        for row in reader:
            testlist.append(row[0])

    return testlist

def getAllIdList(path):

    idlist = list()

    with open(path, "r", encoding="utf-8") as file:
        reader = csv.reader(file)
        next(reader, None)
        for row in reader:
            idlist.append(row[0])

    return idlist

def getTestIndex(entry, list):

    for i, item in enumerate(list):
        if entry == item:
            return i

    return -1

def buildSWLikeTestDataset(all_data, all_labels, all_id):

    totest = getListToTest("overallRunSW.csv")

    test = list()
    labels = list()
    idlist = list()

    for id in totest:
        index = getTestIndex(id, all_id)
        test.append(all_data[index])
        labels.append(all_labels[index])
        idlist.append(id)

    return test, labels, idlist

if __name__ == "__main__":

    classes = ['positive', 'rest_world']

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    test_id = []

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)

    train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld("sentipolc annotation gold v2.csv")

    text_clfPos = Pipeline([('vect', CountVectorizer('word', max_df=0.5, max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True)),
                            ])

    text_clfPos.fit(train_data, train_labels)
    tPos, tLabelsPos, tPosId = buildSWLikeTestDataset(test_data, test_labels, test_id)
    predictionPos = text_clfPos.predict(tPos)

    print("LinearSVC_PosVsWorld")
    print(classification_report(tLabelsPos, predictionPos))

    train_dataNeg, train_labelsNeg, test_dataNeg, test_labelsNeg, test_idNeg = GetData14.getNegVsWorld("sentipolc annotation gold v2.csv")

    text_clfNeg = Pipeline([('vect', CountVectorizer('word', max_df=0.5, max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True)),
                            ])

    text_clfNeg.fit(train_dataNeg, train_labelsNeg)
    tNeg, tLabelsNeg, tNegId = buildSWLikeTestDataset(test_dataNeg, test_labelsNeg, test_idNeg)
    predictionNeg = text_clfNeg.predict(tNeg)

    print("LinearSVC_NegVsWorld")
    print(classification_report(tLabelsNeg, predictionNeg))

    evalbuilder = Builder("overallRun.csv", "exclude_tweets.txt")

    evalbuilder.getResFile(tPosId, predictionPos, tNegId, predictionNeg)

