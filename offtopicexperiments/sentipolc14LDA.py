from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.decomposition import LatentDirichletAllocation
import GetData14
import os
from time import time
from nltk.corpus import stopwords
from pprint import pprint


def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        print("Topic #%d:" % topic_idx)
        print(" ".join([feature_names[i]
                        for i in topic.argsort()[:-n_top_words - 1:-1]]))
    print()


if __name__ == '__main__':

    parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    filepath = os.path.join(parent_dir, "sentipolc annotation gold v2.csv")

    train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld(
        filepath)

    t0 = time()
    count_vect = CountVectorizer('word', max_df=0.5, max_features=1000, min_df=2, stop_words=stopwords.words('italian'))

    tf = count_vect.fit_transform(train_data)
    print("CountVectorizer built in %0.3fs." % (time() - t0))

    t0 = time()
    lda = LatentDirichletAllocation(n_topics=10, max_iter=5,
                                learning_method='online', learning_offset=50.,
                                random_state=0, n_jobs=-1)

    lda.fit(tf)
    print("LDA fit in %0.3fs." % (time() - t0))

    tf_feature_names = count_vect.get_feature_names()
    print_top_words(lda, tf_feature_names, 20)

    s = "Perche' proprio l'Ici (IMU)?: Nel pot-pourri di tributi e sacrifici presenti nella manovra del governo Monti, qu..."

    s_transformed = count_vect.transform([s])

    res = lda.transform(s_transformed)

    pprint(res)