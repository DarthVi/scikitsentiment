from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from customclassifiers.CustomAnalyzer import TweetAnalyzer
from nltk.corpus import stopwords
import GetData14
import os
from pprint import pprint
import nltk


if __name__ == '__main__':

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)

    parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    filepath = os.path.join(parent_dir, "sentipolc annotation gold v2.csv")

    train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld(
        filepath)

    vect = CountVectorizer(analyzer=TweetAnalyzer(stemmer=stemmer), max_df=0.5, max_features=None, ngram_range=(1, 1))

    vect.fit_transform(train_data, train_labels)

    pprint(vect.get_feature_names())