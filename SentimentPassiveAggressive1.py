from TweetAnalyzer import TweetAnalyzer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
import GetData14
from EvalFileBuilder import Builder
import nltk
from sklearn.linear_model import PassiveAggressiveClassifier

if __name__ == "__main__":

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    test_id = []

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)

    train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld("sentipolc annotation gold v2.csv")

    text_clfPos = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(None, stemmer))),
                            ('tfidf', TfidfTransformer(use_idf=True)),
                            ('clf', PassiveAggressiveClassifier(n_jobs=-1, class_weight='balanced', fit_intercept=True, loss='squared_hinge')),
                            ])

    parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
                  'tfidf__use_idf': (True, False),
                  'clf__alpha': (1e-2, 1e-3),
                  }

    text_clfPos.fit(train_data, train_labels)
    predictionPos = text_clfPos.predict(test_data)

    print("PassiveAggressiveClassifier_PosVsWorld")
    print(classification_report(test_labels, predictionPos))

    train_dataNeg, train_labelsNeg, test_dataNeg, test_labelsNeg, test_idNeg = GetData14.getNegVsWorld("sentipolc annotation gold v2.csv")

    text_clfNeg = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(None, stemmer))),
                            ('tfidf', TfidfTransformer(use_idf=True)),
                            ('clf', PassiveAggressiveClassifier(n_jobs=-1, class_weight='balanced', fit_intercept=True, loss='squared_hinge')),
                            ])

    text_clfNeg.fit(train_dataNeg, train_labelsNeg)
    predictionNeg = text_clfNeg.predict(test_dataNeg)

    print("PassiveAggressiveClassifier_NegVsWorld")
    print(classification_report(test_labelsNeg, predictionNeg))

    evalbuilder = Builder("overallRun.csv")

    evalbuilder.getResFile(test_id, predictionPos, test_idNeg, predictionNeg)