from TweetAnalyzer import TweetAnalyzer
from TreeTagger import TreeTagger
import nltk
import pickle

stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)

ta = TweetAnalyzer(None, stemmer)

tt = TreeTagger(None, stemmer)

pickle.dumps(ta)