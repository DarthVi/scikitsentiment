import unittest
import GetData14
import os
import csv

class GetDataTest(unittest.TestCase):

    """Test per la funzione che permette di ottenere i dati dal .csv"""

    def setUp(self):
        parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
        self.filepath = os.path.join(parent_dir, "sentipolc annotation gold v2.csv")

        #creo un dizionario contenente i contenuti del csv e avente l'id come chiave
        self.filedict = dict()
        # data la presenza di più annotazioni per lo stesso tweet id, per il check delle labels è necessaria una lista
        self.filelist = list()
        with open(self.filepath, encoding="utf-8") as f:
            reader = csv.reader(f)
            next(reader, None)
            for line in reader:
                parsedline = list()

                for data in line:
                    data = ''.join(c if c <= '\uFFFF' else hex(ord(c)) for c in data)
                    parsedline.append(data)

                #if parsedline[0] == '134945200278745088':
                    #print(parsedline)


                self.filelist.append([parsedline[0], parsedline[5], parsedline[6]])
                self.filedict[parsedline[0]] = {'origine': parsedline[1], 'topicpol': parsedline[2], 'tweet': parsedline[3], 'sub': parsedline[4], 'pos': parsedline[5], 'neg': parsedline[6], 'iro': parsedline[7], 'setval': parsedline[8]}

    def selectGetFunction(self, label="posvsworld"):

        """Seleziona l'appropriata funzione per ottenere i dati"""

        if label == "posvsworld":
            return GetData14.getPosVsWorld
        elif label == "negvsworld":
            return GetData14.getNegVsWorld
        else:
            self.fail("label errata")


    def execute_isTrainingTest(self, label="posvsworld"):

        """Funzione che verrà chiamata per verificare che non ci siano dati di training fra i dati di testing"""

        function = self.selectGetFunction(label)

        train_data, train_labels, test_data, test_labels, test_id = function(self.filepath)

        # controllo che fra i dati di training non ci siano i dati di testing
        for id in self.filedict.keys():
            if self.filedict[id]['origine'] == 'test' and GetData14.parseLine(
                    self.filedict[id]['tweet']) in train_data:
                self.fail("test tweet in training data found")

        # controllo che fra i dati di testing non ci siano i dati di training
        for id in self.filedict.keys():
            if self.filedict[id]['origine'] == 'train' and GetData14.parseLine(
                    self.filedict[id]['tweet']) in test_data:
                self.fail("test tweet in training data found")


    def execute_compareLabelsByList(self, label):

        """Effettua il check della corrispondenza fra le annotazioni nel .csv e le label messe nelle liste.
            Usa una lista per fare il confronto."""

        function = self.selectGetFunction(label)
        train_data, train_labels, test_data, test_labels, test_id = function(self.filepath)

        if label == "posvsworld":
            i = 1
            tag = 'positive'
        elif label == "negvsworld":
            i = 2
            tag = 'negative'
        else:
            self.fail("label errata")


        for li in self.filelist:
            id = li[0]
            purgedLine = GetData14.parseLine(self.filedict[id]['tweet'])
            classval = tag if li[i] == 'TRUE' else 'rest_world'

            indexes = self.listOfIndexesMatch(purgedLine, train_data)

            successTrain = False

            for index in indexes:
                if successTrain is True:
                    break
                if train_labels[index] is classval:
                    successTrain = True

            indexes = self.listOfIndexesMatch(purgedLine, test_data)
            successTest = False

            for index in indexes:
                if successTest is True:
                    break
                if test_labels[index] is classval:
                    successTest = True

            if successTrain is False and successTest is False:
                self.fail("Mancata corrispondenza fra le annotazioni del file e le label")


    def indexOfItemInList(self, item, list):

        """Ritorna l'indice della prima entry uguale all'item presente in list. Ritorna None in tutti gli altri casi"""

        for i, entry in enumerate(list):
            if entry == item:
                return i

        return None

    def listOfIndexesMatch(self, item, l):

        """Ritorna una lista di indici a cui corrisponde l'item passato come argomento"""

        indexes = list()

        for i, entry in enumerate(l):
            if entry == item:
                indexes.append(i)

        return indexes

    def test_isTraining(self):
        """Test che non ci siano dati di training fra i dati di testing"""

        #eseguo il test sulle classi 'positive' e 'rest_world'
        self.execute_isTrainingTest("posvsworld")

        #eseguo il test sulle classi 'negative' e 'rest_world'
        self.execute_isTrainingTest("negvsworld")

    def test_checkDataLabelAssociation(self):
        """Controlla che ci sia corretta corrispondenza fra dati e label associate"""

        #test su 'positive' e 'rest_world'
        self.execute_compareLabelsByList("posvsworld")

        #test su 'negative' e 'rest_world'
        self.execute_compareLabelsByList("negvsworld")

if __name__ == '__main__':
    unittest.main()