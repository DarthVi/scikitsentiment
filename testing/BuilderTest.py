import unittest
from EvalFileBuilder import Builder
import GetData14
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn import svm
import os

class BuilderTest(unittest.TestCase):

    """Classe usata per testare la corretta creazione del file .csv contenente i risultati"""

    def setUp(self):

        """Carica dati di test e relativi id e label, classifica e memorizza i risultati.
           self.filepath: file che contiene il dataset su cui ci sono i dati di training e testing;
           self.testPos_labels: file che contiene le originali classificazioni del dato (prese dall'annotazione)
           self.testPos_id: file che contiene gli id dei dati di testing; fra testPos_id e testPos_labels c'è una
                            corrispondenza 1:1;
            self.predictionPos: risultati della classificaizone.

            Per i dati del classificator negativi vs mondo funziona in maniera analoga a quanto precedentemente spiegato"""

        parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
        self.filepath = os.path.join(parent_dir, "sentipolc annotation gold v2.csv")

        train_data, train_labels, test_data, self.testPos_labels, self.testPos_id = GetData14.getPosVsWorld(
            self.filepath)

        text_clfPos = Pipeline([('vect', CountVectorizer('word', max_df=0.5, max_features=None, ngram_range=(1, 2))),
                                ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                                ('clf', svm.LinearSVC(class_weight='balanced', dual=True)),
                                ])

        text_clfPos.fit(train_data, train_labels)
        self.predictionPos = text_clfPos.predict(test_data)

        train_dataNeg, train_labelsNeg, test_dataNeg, self.testNeg_labels, self.testNeg_id = GetData14.getNegVsWorld(
            self.filepath)

        text_clfNeg = Pipeline([('vect', CountVectorizer('word', max_df=0.5, max_features=None, ngram_range=(1, 2))),
                                ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                                ('clf', svm.LinearSVC(class_weight='balanced', dual=True)),
                                ])

        text_clfNeg.fit(train_dataNeg, train_labelsNeg)
        self.predictionNeg = text_clfNeg.predict(test_dataNeg)

        evalbuilder = Builder("overallRun.csv", "exclude_tweets.txt")

        evalbuilder.getResFile(self.testPos_id, self.predictionPos, self.testNeg_id, self.predictionNeg)

        self.buildCompareDict("overallRun.csv")

    def buildCompareDict(self, path):
        """Crea il dizionario contente i risultati letti dal file"""

        self.rundata = dict()

        with open(path, "r", encoding="utf-8") as resfile:
            for line in resfile:
                id, sub, pos, neg, iro, top = map(lambda x: x[1:-1], line.rstrip().split(','))
                self.rundata[id] = {'sub':sub, 'pos':pos, 'neg':neg, 'iro':iro}


    def test_outputFile(self):
        """Testa che il file overallRun.csv prodotto da Builder corrisponda ai risultati dei due classificatori"""

        #confronta predictionPos (ossia i risultati del classificatore) con il file finale
        for i, id in enumerate(self.testPos_id):
            valinlist = None

            if self.predictionPos[i] == 'positive':
                valinlist = '1'
            else:
                valinlist = '0'

            if self.rundata[id]['pos'] != valinlist:
                self.fail("il file di output della run non coincide con i risultati dei classificatori")

        #confronta predictionNeg (ossia i risultati del classificatore) con il file finale
        for i, id in enumerate(self.testNeg_id):
            valinlist = None

            if self.predictionNeg[i] == 'negative':
                valinlist = '1'
            else:
                valinlist = '0'

            if self.rundata[id]['neg'] != valinlist:
                self.fail("il file di output della run non coincide con i risultati dei classificatori")


if __name__ == '__main__':
    unittest.main()
