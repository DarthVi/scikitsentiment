import unittest
from customclassifiers.EmojiNaiveClassifier import  EmojiClassifier


class EmojiClassifierTest(unittest.TestCase):

    def setUp(self):
        self.clf = EmojiClassifier()

    def test_classification(self):
        text = ['Life\'s but a walking shadow :(', 'Knowledge is power! :D', 'What o_o', 'sad is sad :D', 'happy post :D']
        labels = ['negative', 'positive', 'negative', 'negative', 'positive']

        self.clf.fit(text, labels)
        test = ["Let's play a game :D", "Sigh :("]
        classification = self.clf.predict(test)

        assert classification[0]=='positive' and classification[1] == 'negative'

    def test_Xnotlonglike_y(self):
        text = ['Life\'s but a walking shadow :(', 'Knowledge is power! :D', 'What o_o', 'sad is sad :D',
                'happy post :D']
        labels = ['negative', 'positive', 'negative', 'negative']

        self.assertRaises(ValueError, self.clf.fit, text, labels)

    def test_unfitted(self):
        self.assertRaises(RuntimeError, self.clf.predict, "Life's but a walking shadow :(")

    def test_nottext(self):
        text = ['Life\'s but a walking shadow :(', 1.03, 'What o_o', 'sad is sad :D',
                'happy post :D']
        labels = ['negative', 'positive', 'negative', 'negative', 'positive']

        self.assertRaises(TypeError, self.clf.fit, text, labels)

    def test_score(self):
        text = ['Life\'s but a walking shadow :(', 'Knowledge is power! :D', 'What o_o', 'sad is sad :D',
                'happy post :D']
        labels = ['negative', 'positive', 'negative', 'negative', 'positive']

        self.clf.fit(text, labels)
        test = ["Let's play a game :D", "Sigh :("]
        accuracy = self.clf.score(test, ['positive', 'negative'])
        assert accuracy == 1.0

    def test_probas(self):
        text = ['Life\'s but a walking shadow :(', 'Knowledge is power! :D', 'What o_o', 'sad is sad :D',
                'happy post :D']
        labels = ['negative', 'positive', 'negative', 'negative', 'positive']

        self.clf.fit(text, labels)
        test = ["Let's play a game :D", "Sigh :("]
        probas = self.clf.predict_proba(test)
        assert probas is not None
        print(list(zip(self.clf.classes_,probas)))