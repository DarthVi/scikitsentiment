import csv

with open("/home/darthvi/SentimentAnalisys_0.3/training_set_sentipolc16.csv", encoding="utf-8") as csvfile:
    reader = csv.reader(csvfile)
    next(reader, None)
    counter = 0
    for row in reader:
        purged = []
        l = list()
        for data in row:
            l.extend([hex(ord(c)) for c in data if c > '\uFFFF'])
        if len(l) is not 0:
            counter += 1
            print(str(counter) + ".", row[0], l)
    print("analysis ended")
