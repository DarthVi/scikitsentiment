<? header("Content-Type: text/html; charset=UTF-8"); ?>

<?php

	class WebserviceClient
	{
		private $url;
		private $username;
		private $password;

		public function __construct($url = "https://localhost:8888", $username, $password)
		{
			$this->url = $url;
			$this->username = $username;
			$this->password = base64_encode($password);
		}

		public function getPassword()
		{
			return base64_decode($this->password);
		}

		/**
		 * Setta la password da usare nelle richieste, memorizzandola come encoding Base64.

         * Base64 per offuscare la password; per quanto la security through obscurity sia una pratica comune
         * suggerirei a chi integra queste API nel suo software, di usare un meccanismo che legga le password (magari
         * a loro volta crittografate) da un file, sottoposto alle policy di sicurezza dell'OS, e passarle solo dopo
         * a questo metodo; dato che queste API riguardano solo la comunicazione con il webservice, ho deciso di
         * non occuparmi di questo problema.

         * La comunicazione con il server avviene tramite protocollo https (con verifica del certificato disabilitata
         * di default perché non rilasciato da una CA, ma autogenerato) e server-side non ci sono problemi di security,
         * visto che le password vengono memorizzate con tecniche apposite di salted hashing.
         *
         * @param string $password
		 */
		public function setPassword($password)
		{
			$this->password = base64_encode($password);
		}

		public function setUsername($username)
		{
			$this->username = $username;
		}

		public function getUsername()
		{
			return $this->username;
		}

		/**
		 * Registra l'utente al servizio
		 * @return json $response
		 */
		public function register()
		{
			$curl = curl_init($this->url . "/api/register");

			$data = array('username'=>$this->getUsername(),'password'=>$this->getPassword());
			$data_json = json_encode($data);

			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

			curl_setopt($curl, CURLOPT_POSTFIELDS,$data_json);

			//Return response instead of printing.
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;
		}

		/**
		 * Effettua il login, in modo tale che le strutture dati riservate all'utente vengano allocate e mantenute
		 * @return json $response
		 */
		public function login()
		{
			$curl = curl_init($this->url . "/login");
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword());
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;
		}

		/**
		 * Effettua il training dei classificatori usando il file di sentipolc 2014
		 * @param float $wtextpos
		 * @param float $wemopos
		 * @param flaot $wtextneg
		 * @param float $wemoneg
		 * @return json $response
		 */
		public function exec_train14($wtextpos=0.0, $wemopos=0.0, $wtextneg=0.0, $wemoneg=0.0)
		{
			$data = array('wtextpos'=>$wtextpos, 'wemopos'=>$wemopos, 'wtextneg'=>$wtextneg, 'wemoneg'=>$wemoneg);
			//$data_json = json_encode($data)

			//$qry_str = "?wtextpos={$wtextpos}&wemopos={$wemopos}&wtextneg={$wtextneg}&wemoneg={$wemoneg}";

			$curl = curl_init($this->url . "/set/train14?" . http_build_query($data, ' ', "&"));
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword());
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;
		}

		/**
		 * Effettua il training dei classificatori usando il file di sentipolc 2016
		 * @param float $wtextpos
		 * @param float $wemopos
		 * @param flaot $wtextneg
		 * @param float $wemoneg
		 * @return json $response
		 */
		public function exec_train16($wtextpos=0.0, $wemopos=0.0, $wtextneg=0.0, $wemoneg=0.0)
		{
			$data = array('wtextpos'=>$wtextpos, 'wemopos'=>$wemopos, 'wtextneg'=>$wtextneg, 'wemoneg'=>$wemoneg);

			$curl = curl_init($this->url . "/set/train16?" . http_build_query($data, '', "&"));
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword());
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;
		}

		/**
		 * Ottiene la classificazione di un singolo testo
		 * @param string $text
		 * @return json $response
		 */
		public function get_text_classification($text)
		{
			//$data = array('text'=>$text);
			//$data_json = json_encode($data);
			$data = "text=" .  $text;

			$curl = curl_init($this->url . "/get/classification/text");

			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword());

			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));

			curl_setopt($curl, CURLOPT_POSTFIELDS,$data);

			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;

		}

		/**
		 * Effettua il logout per deallocare le risorse riservate all'utente
		 * @return json $response
		 */
		public function logout()
		{
			$curl = curl_init($this->url . "/logout");
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword());
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;
		}

		/**
		 * Addestra i classificatori con il file fornito.
		 * Il file .csv deve essere nel seguente formato
		 *
		 * prima riga: "testo","subj","opos","oneg"
		 * esempi dalla seconda in poi:
		 * "testo1",1,1,0
		 * "testo2",1,0,1
		 *
		 * @param string $filepath path assoluto (compreso filename ed estensione) del file da caricare
		 * @param float $wtextpos
		 * @param float $wemopos
		 * @param flaot $wtextneg
		 * @param float $wemoneg
		 * @return json $response
		 */
		public function train($filepath, $wtextpos=0.0, $wemopos=0.0, $wtextneg=0.0, $wemoneg=0.0)
		{
			$data = array('wtextpos'=>$wtextpos, 'wemopos'=>$wemopos, 'wtextneg'=>$wtextneg, 'wemoneg'=>$wemoneg, 'file_train' => '@' . $filepath);

			$curl = curl_init($this->url . "/train");
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword());
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;
		}

		/**
		 * Setta il classificatore del testo per la label scelta con l'apposita tecnica scelta
		 * @param string $label scegli quale classificatore cambiare, "pos" o "neg"
		 * @param string $clfstring scegli il tipo di classificatore ("linearsvc" o "multinomialnb")
		 * @return json $response
		 */
		public function set_clftext($label, $clfstring)
		{
			$curl = curl_init($this->url . "/set/text/{$label}/{$clfstring}");
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword());
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;
		}

		/**
		 * Setta il classificatore delle emoticon con l'apposita scelta
		 * @param string $label scegli quale classificatore cambiare, "pos" o "neg"
		 * @param string $clfstring scegli se usare tfidf o notfidf
		 * @return json $response
		 */
		public function set_clfemoji($label, $config)
		{
			$curl = curl_init($this->url . "/set/emo/{$label}/{$config}");
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword());
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;
		}

		/**
		 * Salva il modello appreso
		 * @return json $response
		 */
		public function savemodel()
		{
			$curl = curl_init($this->url . "/savemodel");
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword());
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;
		}

		/**
		 * Carica il modello precedentemente appreso
		 * @return json $response
		 */
		public function loadmodel()
		{
			$curl = curl_init($this->url . "/loadmodel");
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ":" . $this->getPassword());
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

			$response  = curl_exec($curl);
			curl_close($curl);
			return $response;
		}

	}
?>