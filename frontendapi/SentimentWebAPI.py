import requests
import base64
import json


class WebserviceClient:
    def __init__(self, address="https://localhost", port="8888", username=None, password=None,
                 verify_cert=False):
        self.address = address
        self.port = port
        self.username = username
        self.verify_cert = verify_cert

        if password is not None:
            self.setpassword(password)
        else:
            self.password = password

    def setusername(self, username):
        self.username = username

    def setpassword(self, password):
        """
        Setta la password da usare nelle richieste, memorizzandola come encoding Base64.

        Base64 per offuscare la password; per quanto la security through obscurity sia una pratica comune
        suggerirei a chi integra queste API nel suo software, di usare un meccanismo che legga le password (magari
        a loro volta crittografate) da un file, sottoposto alle policy di sicurezza dell'OS, e passarle solo dopo
        a questo metodo; dato che queste API riguardano solo la comunicazione con il webservice, ho deciso di
        non occuparmi di questo problema.

        La comunicazione con il server avviene tramite protocollo https (con verifica del certificato disabilitata
        di default perché non rilasciato da una CA, ma autogenerato) e server-side non ci sono problemi di security,
        visto che le password vengono memorizzate con tecniche apposite di salted hashing.
        :param password:
        :return:
        """
        self.password = base64.b64encode(password.encode())

    def getpassword(self):
        return base64.b64decode(self.password).decode("utf-8")

    def register(self):

        """
        Registra l'utente al servizio
        :return: json di response
        """
        headers = {'Content-Type': 'application/json; charset=UTF-8'}
        payload = json.dumps({'username': self.username, 'password': self.getpassword()})
        r = requests.post(self.address + ":" + self.port + "/api/register",
                          data=payload, headers=headers, verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def login(self):

        """
        Effettua il login, in modo tale che le strutture dati riservate all'utente vengano allocate e mantenute
        :return: json di response
        """

        r = requests.get(self.address + ":" + self.port + "/login", auth=(self.username, self.getpassword()),
                         verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def logout(self):

        """
        Effettua il logout, per deallocare le risorse riservate allutente
        :return: json di response
        """
        r = requests.get(self.address + ":" + self.port + "/logout", auth=(self.username, self.getpassword()),
                         verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def set_clftext(self, label, clfstring):

        """
        Setta il classificatore per la label scelta con l'apposita tecnica scelta
        :param label: scegli quale classificatore cambiare (quello sui positivi vs world, identificato da pos, o
                      negativi vs world, identificato da neg)
        :param clfstring: scegli il tipo di classificatore (linearsvc o mulinomialnb)
        :return: jsone di response
        """
        r = requests.get(self.address + ":" + self.port + "/set/text/{0}/{1}".format(label, clfstring),
                         auth=(self.username, self.getpassword()),
                         verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def set_clfemoji(self, label, config):

        """
        Setta il classificatore per le emoticon con l'apposita scelta
        :param label: scegli quale classificatore cambiare (quello sui positivi vs world, identificato da pos, o
                      negativi vs world, identificato da neg)
        :param config: scegli se usare tfidf o notfidf (il parametro va definito esattamente così come è scritto,
                       come stringa)
        :return: json di response
        """
        r = requests.get(self.address + ":" + self.port + "/set/emo/{0}/{1}".format(label, config),
                         auth=(self.username, self.getpassword()),
                         verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def exec_train14(self, wtextpos=0.0, wemopos=0.0, wtextneg=0.0, wemoneg=0.0):

        """
        Effettua il training dei classificatori usando il file di sentipolc 2014
        :return: json di response
        """

        payload = {'wtextpos': wtextpos, 'wemopos': wemopos, 'wtextneg': wtextneg, 'wemoneg': wemoneg}

        r = requests.get(self.address + ":" + self.port + "/set/train14",
                         auth=(self.username, self.getpassword()), params=payload,
                         verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def exec_train16(self, wtextpos=0.0, wemopos=0.0, wtextneg=0.0, wemoneg=0.0):

        """
        Effettua il training dei classificatori usando il file di sentipolc 2016
        :return: json di response
        """
        payload = {'wtextpos': wtextpos, 'wemopos': wemopos, 'wtextneg': wtextneg, 'wemoneg': wemoneg}

        r = requests.get(self.address + ":" + self.port + "/set/train16",
                         auth=(self.username, self.getpassword()), params=payload,
                         verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def savemodel(self):

        """
        Salva il modello appreso
        :return: json di response
        """

        r = requests.get(self.address + ":" + self.port + "/savemodel",
                         auth=(self.username, self.getpassword()),
                         verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def loadmodel(self):

        """
        Carica il modello precedentemente appreso
        :return: json di response
        """

        r = requests.get(self.address + ":" + self.port + "/loadmodel",
                         auth=(self.username, self.getpassword()),
                         verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def test_sentipolc14(self):

        """
        Effettua il test del dataset di testing di sentipolc 2014
        :return: json di response
        """

        r = requests.get(self.address + ":" + self.port + "/test/sentipolc14",
                         auth=(self.username, self.getpassword()),
                         verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def get_result_sentipolc(self, filepath):

        """
        Ottiene i risultati della run di test di sentipolc
        :return: file di response
        :param filepath: path in cui salvare il risultato
        """

        with open(filepath, 'wb') as handle:
            response = requests.get(self.address + ":" + self.port + "/get/sentipolc/result",
                                    auth=(self.username, self.getpassword()), stream=True,
                                    verify=self.verify_cert)

            if not response.ok:
                return json.dumps({'username': self.username, 'success': False, 'error': response.status_code})

            for block in response.iter_content(1024):
                handle.write(block)

            return json.dumps({'username': self.username, 'success': True, 'path': filepath})

    def train(self, filepath, wtextpos=0.0, wemopos=0.0, wtextneg=0.0, wemoneg=0.0):

        """
        Addestra i classificatori con il file fornito

        Il file .csv deve essere nel seguente formato.

        prima riga: "testo","subj","opos","oneg"
        esempi dalla seconda in poi:

        "testo1",1,1,0
        "testo2",1,0,1

        :param filepath: filepath del file scelto
        :return: json di risposta
        """

        payload = {'wtextpos': wtextpos, 'wemopos': wemopos, 'wtextneg': wtextneg, 'wemoneg': wemoneg}

        files = {'file_train': open(filepath, 'rb')}
        r = requests.post(self.address + ":" + self.port + "/train",
                          auth=(self.username, self.getpassword()),
                          verify=self.verify_cert, files=files, data=payload)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def get_text_classification(self, text):

        """
        Ottiene la classificazione di un singolo testo
        :param text:
        :return: json contenente i dati di classificazione
        """
        payload = {'text': text}
        r = requests.post(self.address + ":" + self.port + "/get/classification/text",
                          auth=(self.username, self.getpassword()),
                          verify=self.verify_cert, data=payload)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def get_classification_by_file(self, filepath):

        """
        Ottiene la classificazione di un insieme di testi presenti all'interno di un file .csv
        formattato secondo lo stile "id","testo".

        :param filepath:
        :return: file json di risposta contenente le classificazioni
        """
        files = {'file_test': open(filepath, 'rb')}
        r = requests.post(self.address + ":" + self.port + "/get/classification/file",
                          auth=(self.username, self.getpassword()),
                          verify=self.verify_cert, files=files)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def get_probability_file(self, filepath):

        """
        Ottiene le probabilità di appartenenza alle classi di ogni testo presente all'interno
        di un file .csv formattato secondo lo stile "id","testo"

        :param filepath:
        :return: file json di risposta contenente le classificazioni
        """

        files = {'file_test': open(filepath, 'rb')}
        r = requests.post(self.address + ":" + self.port + "/get/proba/file",
                          auth=(self.username, self.getpassword()),
                          verify=self.verify_cert, files=files)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def get_probability_text(self, text):

        """
        Ottiene le probabilità di appartenenza alle varie classi
        :param text:
        :return: json contenente i dati di classificazione
        """
        payload = {'text': text}
        r = requests.post(self.address + ":" + self.port + "/get/proba/text",
                          auth=(self.username, self.getpassword()),
                          verify=self.verify_cert, data=payload)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()

    def get_sentipolc14_proba(self):

        """
        Ottiene le probabilità di appartenenza alle varie classi per quanto riguarda sentipolc2014
        :param text:
        :return: json contenente i dati di classificazione
        """
        r = requests.get(self.address + ":" + self.port + "/get/sentipolc14/proba",
                         auth=(self.username, self.getpassword()),
                         verify=self.verify_cert)

        if r.status_code != requests.codes.ok:
            return json.dumps({'username': self.username, 'success': False, 'error': r.status_code})
        else:
            return r.json()



# da usare solo per testing informale durante il development
#if __name__ == '__main__':
#    client = WebserviceClient(username='FordPrefect2', password='42larisposta')
#    print(client.login())
#    print(client.train(filepath="/home/darthvi/Documenti/test_train.csv"))
