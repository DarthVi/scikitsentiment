import sqlite3 as sqlite


class SecDataDao(object):

    """
    Data Access Object da usare per accedere alle funzionalità di persistenza dei dati degli utenti
    (username e password)
    """

    def __init__(self, path='secdata.db'):
        self.path = path

        connection = sqlite.connect(self.path)
        conn = connection.cursor()

        try:
            conn.execute("CREATE TABLE IF NOT EXISTS db_sec (uname text PRIMARY KEY, upwd TEXT)")
            connection.commit()
        finally:
            connection.close()

    def insert(self, uname, upwd):
        """
        Inserisce una nuova coppia (username, password)
        :param uname:
        :param upwd:
        :return:
        """

        connection = sqlite.connect(self.path)
        conn = connection.cursor()

        try:
            conn.execute("INSERT INTO db_sec VALUES (?, ?)", (uname, upwd))
            connection.commit()
        finally:
            connection.close()

    def get(self, uname):
        """
        Ottiene la tupla (username, password) utilizzando la chiave (uname) fornita come parametro del metodo
        :param uname:
        :return:
        """
        connection = sqlite.connect(self.path)
        conn = connection.cursor()

        try:
            conn.execute("SELECT * from db_sec WHERE uname=:id", {'id': uname})
            return conn.fetchone()
        finally:
            connection.close()


# if __name__ == '__main__':
#     dao = SecDataDao()
#     dao.insert("giorgio", "lol")
#     name, pwd = dao.get("giorgio")
#     print(name, pwd)