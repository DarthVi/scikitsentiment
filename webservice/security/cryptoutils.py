from passlib.apps import custom_app_context as pwd_context

class CryptoUtil:

    """
    Wrapper per le funzionalità di hashing delle password; usa passlib
    """

    @staticmethod
    def encrypt(text):
        hash_ = pwd_context.encrypt(text)
        return hash_

    @staticmethod
    def verify(pwd, hash_):
        return pwd_context.verify(pwd, hash_)
