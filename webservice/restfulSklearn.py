from bottle import Bottle, abort, request, debug, server_names, ServerAdapter, auth_basic, static_file
import os
import base64
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.externals import joblib
from sklearn.base import clone
import GetData
from sklearn.ensemble import VotingClassifier
from customclassifiers.EmojiCustomClassifier import EmojiCustomClassifier
import GetData14
import GetData16
from EvalFileBuilder import Builder
import nltk
from sklearn import svm
from sklearn.preprocessing import LabelEncoder
from customclassifiers.CustomAnalyzer import TweetAnalyzer
import argparse
from webservice.security.cryptoutils import CryptoUtil
from webservice.dao.regdao import SecDataDao

parser = argparse.ArgumentParser(description='')
parser.add_argument('-p', dest="port", help="porta su cui sarà in ascolto il web service", type=int, default=8888,
                    action="store")
parser.add_argument('-a', dest="host", help="host del web service", default="localhost", action="store")
args = parser.parse_args()


### il codice seguente è una versione leggermente modificata dal seguente link
# http://dgtool.treitos.com/2011/12/ssl-encryption-in-python-bottle.html

# Declaration of new class that inherits from ServerAdapter
# It's almost equal to the supported cherrypy class CherryPyServer
class MySSLCherryPy(ServerAdapter):
    """
    Serve ad abilitare l'uso di CherryPy, WSGI server multi-threaded, e ad aggiungergli il supporto ad
    SSL, dando la possibilità agli utenti di collegarsi al web service tramite https
    """

    def run(self, handler):
        from cherrypy import wsgiserver
        from cherrypy.wsgiserver.ssl_builtin import BuiltinSSLAdapter
        server = wsgiserver.CherryPyWSGIServer((self.host, self.port), handler)

        # If cert variable is has a valid path, SSL will be used
        # You can set it to None to disable SSL
        cert = 'server.pem'  # certificate path
        key = 'server.key'
        server.ssl_adapter = BuiltinSSLAdapter(cert, key, None)
        try:
            server.start()
        finally:
            server.stop()


# Add our new MySSLCherryPy class to the supported servers
# under the key 'mysslcherrypy'
server_names['mysslcherrypy'] = MySSLCherryPy


# fine del codice di dgtool

def check_auth(username, password):
    """
    Funzione usata per verificare le credenziali dell'utente quando chiede di accedere a determinati servizi
    che richiedono l'autenticazione
    :param username:
    :param password:
    :return:
    """
    dao = SecDataDao()

    try:
        (name, hashpwd) = dao.get(username)
        return CryptoUtil.verify(password, hashpwd)
    except:
        return False


# TODO: disattivare il debuggin superata la fasa dei prototipazione e development
debug(True)


class SentimentWebService(object):
    """
    Classe principale usata dal webservice: usa Bottle per esporre i metodi.
    Ci sono due tipi di classificatori di base: LinearSVC e MultinomialNB.
    Gli utenti possono decidere quale usare tramite le apposite chiamate: dal login in poi o dal setting manuale
    precedentemente menzionato, gli utenti utilizzerano il loro specifico classificatore. Di default dopo
    dopo la registrazione, vengono assegnati LinearSVC per il testo e EmojiCustomClassifier (con tfidf abilitato) per
    le emoji. Dopo essersi registrato l'utente potrà facilmente usare le altre chiamate inviando i dettagli di
    usati per la registrazione.

    Per l'autenticazione viene usato HTTP Basic Authentication (vedere rfc1945), nel database sqlite vengono salvate
    username e password, queste ultime dopo esser state hashate con passlib, ovviamente usando
    tecniche apposite. C'è un dao (regdao.py) che gestisce la comunicazione con il database.
    """

    def __init__(self, host, port, server):

        """
        Inizializza le strutture dati di base
        :param host:
        :param port:
        :param server:
        """
        self.stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)

        self.svc_clfPos = Pipeline(
            [('vect', CountVectorizer(analyzer='word', max_df=0.5, max_features=None, ngram_range=(1, 2))),
             ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
             ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=False, max_iter=1000)),
             ])
        # self.emo_clfPos = EmojiCustomClassifier()
        self.emo_clfPos = dict()

        self.svc_clfNeg = Pipeline(
            [('vect', CountVectorizer(analyzer='word', max_df=0.5, max_features=None, ngram_range=(1, 2))),
             ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
             ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=True, max_iter=1000)),
             ])

        # self.emo_clfNeg = EmojiCustomClassifier()
        self.emo_clfNeg = dict()

        self.mbayes_clfPos = Pipeline(
            [('vect', CountVectorizer(analyzer=TweetAnalyzer(stemmer=self.stemmer), max_df=0.5,
                                      max_features=None, ngram_range=(1, 1))),
             ('clf', MultinomialNB(alpha=1.0, fit_prior=True)),
             ])
        self.mbayes_clfNeg = Pipeline(
            [('vect', CountVectorizer(analyzer='word', max_df=0.5, max_features=None, ngram_range=(1, 2))),
             ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
             ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=True, max_iter=1000)),
             ])

        # self.clfPos = self.svc_clfPos
        # self.clfNeg = self.svc_clfNeg
        self.clfPos = dict()
        self.clfNeg = dict()

        self.votingclf_Pos = dict()
        self.votingclf_Neg = dict()

        self.host_ = host
        self.port_ = port
        self.server_ = server
        self.app_ = Bottle()
        self.route_()

    def get_user_from_auth(self, authstring):

        """
        Ottiene la username dalla stringa contenuta nell'http header "Authorization"
        :param authstring:
        :return:
        """

        data = authstring[len("Basic "):]
        decoded = base64.b64decode(data).decode("utf-8")

        user = decoded.split(":")[0]

        return user

    def route_(self):

        """
        Definisce i possibili url accessibili per le chiamate al web service fatte seguendo il principio
        architetturale REST.
        Per ogni route c'è l'url e la funzione di callback da eseguire qualora si acceda all'url specificato
        :return:
        """
        self.app_.route('/set/text/<label>/<name>', callback=self.set_textclf)
        self.app_.route('/set/emo/<label>/<config>', callback=self.set_emojitf)
        self.app_.route('/set/train14', callback=self.train_sentipolc14)
        self.app_.route('/set/train16', callback=self.train_sentipolc16)
        self.app_.route('/login', callback=self.login)
        self.app_.route('/savemodel', callback=self.savemodel)
        self.app_.route('/loadmodel', callback=self.loadmodel)
        self.app_.route('/api/register', callback=self.register, method='POST')
        self.app_.route('/logout', callback=self.logout)
        self.app_.route('/train', callback=self.train, method='POST')
        self.app_.route('/test/sentipolc14', callback=self.test_sentipolc14)
        self.app_.route('/get/sentipolc/result', callback=self.get_sentipolc_result)
        self.app_.route('/get/classification/text', callback=self.get_classification, method='POST')
        self.app_.route('/get/classification/file', callback=self.get_classification_by_file, method='POST')
        self.app_.route('/get/sentipolc14/proba', callback=self.get_sentipolc14_probability)
        self.app_.route('/get/proba/text', callback=self.get_probability_text, method='POST')
        self.app_.route('/get/proba/file', callback=self.get_probability_file, method='POST')
        self.app_.route('/robots.txt', callback=self.serve_robots)

    def start(self):
        """
        Avvia il server
        :return:
        """
        self.app_.run(host=self.host_, port=self.port_, server=self.server_)

    def serve_robots(self):
        return static_file('robots.txt', root="./")

    def register(self):
        """
        Effettua la registrazione dell'utente, utilizzando username e password fornite sotto forma di json
        tramite metodo POST
        :return:
        """
        username = request.json.get('username')
        password = request.json.get('password')

        passwd = CryptoUtil.encrypt(password)
        dao = SecDataDao()

        dao.insert(username, passwd)

        # inizializza le strutture dati dell'utente registrato
        self.clfPos[username] = clone(self.svc_clfPos)
        self.clfNeg[username] = clone(self.svc_clfNeg)
        self.emo_clfPos[username] = EmojiCustomClassifier()
        self.emo_clfNeg[username] = EmojiCustomClassifier()

        return {'username': username, 'success': True}

    @auth_basic(check_auth)
    def login(self):

        """Il login serve ad inizializzare le strutture dati (classificatori) che dovrà usare l'utente"""

        # ottiene l'username dell'utente tramite l'header Authorization
        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        # inizializza i classificatori
        self.clfPos[user] = clone(self.svc_clfPos)
        self.clfNeg[user] = clone(self.svc_clfNeg)
        self.emo_clfPos[user] = EmojiCustomClassifier()
        self.emo_clfNeg[user] = EmojiCustomClassifier()

        return {'username': user, 'success': True}

    @auth_basic(check_auth)
    def set_textclf(self, label, name):

        """
        Sceglie quali classificatori usare (linearsvc o multinomialnb)
        :param label:
        :param name:
        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        respdict = dict()

        try:
            del self.clfPos[user]
            del self.clfNeg[user]
        except KeyError:
            pass  # TODO: gestire eccezione?

        if label == 'pos':
            if name == 'linearsvc':
                self.clfPos[user] = clone(self.svc_clfPos)
                respdict['text_pos'] = 'linearsvc'
            elif name == 'multinomialnb':
                self.clfPos[user] = clone(self.mbayes_clfPos)
                respdict['text_pos'] = 'multinomialnb'
            return respdict
        elif label == 'neg':
            if name == 'linearsvc':
                self.clfNeg[user] = clone(self.svc_clfNeg)
                respdict['text_neg'] = 'linearsvc'
            elif name == 'multinomialnb':
                self.clfNeg[user] = clone(self.mbayes_clfNeg)
                respdict['text_neg'] = 'multinomialnb'
            return respdict
        else:
            abort(400, 'Bad request: dopo /set/text/ usare pos e neg seguiti da linearsvc o multinomialnb\n'
                       'esempio - (/set/pos/linearsvc')

    @auth_basic(check_auth)
    def set_emojitf(self, label, config):

        """
        Sceglie, per ogni label (pos e neg), se usare o meno tfidf per il classificatore dedicato alle emoticon
        :param label:
        :param config:
        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        respdict = dict()

        if label == 'pos':
            if config == 'tfidf':
                self.emo_clfPos[user] = EmojiCustomClassifier()
                respdict['emo_pos'] = 'tfidf'
            elif config == 'notfidf':
                self.emo_clfPos[user] = EmojiCustomClassifier(use_tfidf=False)
                respdict['emo_pos'] = 'notfidf'
            return respdict
        elif label == 'neg':
            if config == 'tfidf':
                self.emo_clfNeg[user] = EmojiCustomClassifier()
                respdict['emo_neg'] = 'tfidf'
            elif config == 'notfidf':
                self.emo_clfNeg[user] = EmojiCustomClassifier(use_tfidf=False)
                respdict['emo_neg'] = 'notfidf'
            return respdict
        else:
            abort(400, 'Bad request: dopo /set/emo/ usare pos e neg seguiti da tfidf o notfidf\n'
                       'esempio - (/set/pos/tfidf')

    @auth_basic(check_auth)
    def train_sentipolc14(self):
        """
        Addestra i classificatori usando il file di sentipolc 2014 e salva il modello
        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        try:
            del self.votingclf_Neg[user]
            del self.votingclf_Neg[user]
        except KeyError:
            pass  # TODO: fare qualcosa qui?

        save = request.query.save or '1'
        wtextpos = float(request.query.wtextpos) or 0.0
        wemopos = float(request.query.wemopos) or 0.0
        wtextneg = float(request.query.wtextneg) or 0.0
        wemoneg = float(request.query.wemoneg) or 0.0

        parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
        filepath = os.path.join(parent_dir, "sentipolc annotation gold v2.csv")

        train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld(
            filepath)

        if wtextpos == 0 and wemopos == 0:
            self.votingclf_Pos[user] = VotingClassifier(estimators=[('text_clfPos', self.clfPos[user]),
                                                                ('emo_clfPos', self.emo_clfPos[user])], voting='hard')
        else:
            self.votingclf_Pos[user] = VotingClassifier(estimators=[('text_clfPos', self.clfPos[user]),
                                                                    ('emo_clfPos', self.emo_clfPos[user])],
                                                        voting='soft', weights=[wtextpos, wemopos])

        self.votingclf_Pos[user].fit(train_data, train_labels)

        train_dataNeg, train_labelsNeg, test_dataNeg, test_labelsNeg, test_idNeg = GetData14.getNegVsWorld(
            filepath)

        if wtextneg == 0 and wemoneg == 0:
            self.votingclf_Neg[user] = VotingClassifier(estimators=[('text_clfNeg', self.clfNeg[user]),
                                                                ('emo_clfNeg', self.emo_clfNeg[user])], voting='hard')
        else:
            self.votingclf_Neg[user] = VotingClassifier(estimators=[('text_clfNeg', self.clfNeg[user]),
                                                                    ('emo_clfNeg', self.emo_clfNeg[user])],
                                                        voting='soft', weights=[wtextneg, wemoneg])

        self.votingclf_Neg[user].fit(train_dataNeg, train_labelsNeg)

        respdict = dict()

        if save == '1':
            respdict = self.dumpmodel(user)

        respdict['train_type'] = 'train_sentipolc2014'
        respdict['pos_type'] = str(type(self.clfPos[user].named_steps['clf']))
        respdict['neg_type'] = str(type(self.clfNeg[user].named_steps['clf']))

        if self.emo_clfPos[user].get_use_tfidf() is True:
            respdict['emo_pos_tfidf'] = True
        else:
            respdict['emo_pos_tfidf'] = False

        if self.emo_clfNeg[user].get_use_tfidf() is True:
            respdict['emo_neg_tfidf'] = True
        else:
            respdict['emo_neg_tfidf'] = False

        joblib.dump(respdict, user + '_last_train.pkl')

        return respdict

    @auth_basic(check_auth)
    def train_sentipolc16(self):
        """
        Addestra i classificatori usando il file di sentipolc 2014 e salva il modello
        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        save = request.query.save or '1'
        wtextpos = float(request.query.wtextpos) or 0.0
        wemopos = float(request.query.wemopos) or 0.0
        wtextneg = float(request.query.wtextneg) or 0.0
        wemoneg = float(request.query.wemoneg) or 0.0

        parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
        filepath = os.path.join(parent_dir, "training_set_sentipolc16.csv")

        try:
            del self.votingclf_Neg[user]
            del self.votingclf_Neg[user]
        except KeyError:
            pass  # TODO: fare qualcosa qui?

        train_data, train_labels, pos_id = GetData16.getPosVsWorld(
            filepath)

        if wtextpos == 0 and wemopos == 0:
            self.votingclf_Pos[user] = VotingClassifier(estimators=[('text_clfPos', self.clfPos[user]),
                                                                ('emo_clfPos', self.emo_clfPos[user])], voting='hard')
        else:
            self.votingclf_Pos[user] = VotingClassifier(estimators=[('text_clfPos', self.clfPos[user]),
                                                                    ('emo_clfPos', self.emo_clfPos[user])],
                                                        voting='soft', weights=[wtextpos, wemopos])

        self.votingclf_Pos[user].fit(train_data, train_labels)

        train_dataNeg, train_labelsNeg, neg_id = GetData16.getNegVsWorld(
            filepath)

        if wtextneg == 0 and wemoneg == 0:
            self.votingclf_Neg[user] = VotingClassifier(estimators=[('text_clfNeg', self.clfNeg[user]),
                                                                ('emo_clfNeg', self.emo_clfNeg[user])], voting='hard')
        else:
            self.votingclf_Neg[user] = VotingClassifier(estimators=[('text_clfNeg', self.clfNeg[user]),
                                                                    ('emo_clfNeg', self.emo_clfNeg[user])],
                                                        voting='soft', weights=[wtextneg, wemoneg])

        self.votingclf_Neg[user].fit(train_dataNeg, train_labelsNeg)

        respdict = dict()

        if save == '1':
            respdict = self.dumpmodel(user)

        respdict['train_type'] = 'train_sentipolc2016'
        respdict['pos_type'] = str(type(self.clfPos[user].named_steps['clf']))
        respdict['neg_type'] = str(type(self.clfNeg[user].named_steps['clf']))

        if self.emo_clfPos[user].get_use_tfidf() is True:
            respdict['emo_pos_tfidf'] = True
        else:
            respdict['emo_pos_tfidf'] = False

        if self.emo_clfNeg[user].get_use_tfidf() is True:
            respdict['emo_neg_tfidf'] = True
        else:
            respdict['emo_neg_tfidf'] = False

        joblib.dump(respdict, user + '_last_train.pkl')

        return respdict

    def dumpmodel(self, user):

        """
        Funzione di utility usata da altre funzioni per fornire il salvataggio del modello appreso.
        :param user:
        :return:
        """

        respdict = dict()

        dumpPos = './persistence/' + user + '_clfPos.pkl'
        dumpNeg = './persistence/' + user + '_clfNeg.pkl'

        joblib.dump(self.votingclf_Pos[user], dumpPos)
        joblib.dump(self.votingclf_Neg[user], dumpNeg)
        respdict['clf_pos'] = os.path.split(dumpPos)[1]
        respdict['clf_neg'] = os.path.split(dumpNeg)[1]

        return respdict

    @auth_basic(check_auth)
    def savemodel(self):

        """
        Salva il modello di classificatore appreso
        :return:
        """
        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        respdict = self.dumpmodel(user)

        respdict['username'] = user

        return respdict

    @auth_basic(check_auth)
    def loadmodel(self):

        """
        Carica il modello precedentemente salvato. Se non è stato precedentemente salvato, nel try..except
        presente nelle righe di codice successive, verrà generato un IOError e verrà ritornata una stringa
        json che informa l'utente del fallimento dell'operazione.
        Se l'operazione ha successo, verrà caricato il modello e verrà restituito un json che cominichi
        il corretto caricamento.
        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        dumpPos = './persistence/' + user + '_clfPos.pkl'
        dumpNeg = './persistence/' + user + '_clfNeg.pkl'

        try:
            filepos = open(dumpPos)
            fileneg = open(dumpNeg)

            filepos.close()
            fileneg.close()

            try:
                del self.votingclf_Neg[user]
                del self.votingclf_Neg[user]
            except KeyError:
                pass  # TODO: fare qualcosa qui?

            self.votingclf_Pos[user] = joblib.load(dumpPos)
            self.votingclf_Neg[user] = joblib.load(dumpNeg)

            return {'username': user, 'success': True}
        except IOError:
            return {'username': user, 'success': False, 'error': "modello non trovato: assenza di precedente "
                                                                 "salvataggio"}

    @auth_basic(check_auth)
    def logout(self):

        """
        Elimina le strutture dati inizializzate per l'utente in questione
        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        del self.emo_clfPos[user]
        del self.emo_clfNeg[user]
        del self.clfPos[user]
        del self.clfNeg[user]
        del self.votingclf_Pos[user]
        del self.votingclf_Neg[user]

        return {'username': user, 'success': True}

    @auth_basic(check_auth)
    def train(self):

        """
        Addestra il classificatore usando un file .csv inviato dall'utente.
        Il file deve essere nel seguente formato.

        prima riga: "testo","subj","opos","oneg"
        esempi dalla seconda in poi:

        "testo1",1,1,0
        "testo2",1,0,1

        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        file = request.files.get("file_train")
        wtextpos = float(request.forms.get('wtextpos')) or 0.0
        wemopos = float(request.forms.get('wemopos')) or 0.0
        wtextneg = float(request.forms.get('wtextneg')) or 0.0
        wemoneg = float(request.forms.get('wemoneg')) or 0.0

        filepath = "./persistence/" + user + "_file.csv"

        file.save(filepath, overwrite=True)

        datapos, labelpos = GetData.getPosVsWorld(filepath)
        dataneg, labelneg = GetData.getNegVsWorld(filepath)

        try:
            del self.votingclf_Neg[user]
            del self.votingclf_Neg[user]
        except KeyError:
            pass  # TODO: fare qualcosa qui?

        if wtextpos == 0 and wemopos == 0:
            self.votingclf_Pos[user] = VotingClassifier(estimators=[('text_clfPos', self.clfPos[user]),
                                                                ('emo_clfPos', self.emo_clfPos[user])], voting='hard')
        else:
            self.votingclf_Pos[user] = VotingClassifier(estimators=[('text_clfPos', self.clfPos[user]),
                                                                    ('emo_clfPos', self.emo_clfPos[user])],
                                                        voting='soft', weights=[wtextpos, wemopos])

        if wtextneg == 0 and wemoneg == 0:
            self.votingclf_Neg[user] = VotingClassifier(estimators=[('text_clfNeg', self.clfNeg[user]),
                                                                ('emo_clfNeg', self.emo_clfNeg[user])], voting='hard')
        else:
            self.votingclf_Neg[user] = VotingClassifier(estimators=[('text_clfNeg', self.clfNeg[user]),
                                                                    ('emo_clfNeg', self.emo_clfNeg[user])],
                                                        voting='soft', weights=[wtextneg, wemoneg])

        self.votingclf_Pos[user].fit(datapos, labelpos)
        self.votingclf_Neg[user].fit(dataneg, labelneg)

        return {'username': user, 'success': True}

    @auth_basic(check_auth)
    def test_sentipolc14(self):

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
        filepath = os.path.join(parent_dir, "training_set_sentipolc16.csv")

        train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld(
            filepath)

        train_dataNeg, train_labelsNeg, test_dataNeg, test_labelsNeg, test_idNeg = GetData14.getNegVsWorld(
            filepath)

        predictionPos = self.votingclf_Pos[user].predict(test_data)
        predictionNeg = self.votingclf_Neg[user].predict(test_dataNeg)

        evalbuilder = Builder("./persistence/" + user + "_overallRun.csv")

        evalbuilder.getResFile(test_id, predictionPos, test_idNeg, predictionNeg)

    @auth_basic(check_auth)
    def get_sentipolc_result(self):

        """
        Ottiene i risultati dei test fatti secondo le linee guida di sentipolc
        (ottiene un file .csv contenente le classificazioni fatte dai classificatori).
        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        try:
            return static_file(user + "_overallRun.csv", root="./persistence")
        except (RuntimeError, RuntimeWarning):
            abort(404, "File non esistente")

    @auth_basic(check_auth)
    def get_classification(self):

        """
        Ottiene la classificazione di un testo inviato tramite metodo POST
        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        # curl -u user:password -F text=inserire_qui_il_testo http://indirizzo:porta/get/classification/text
        text = request.forms.get("text")

        posres = self.votingclf_Pos[user].predict([text])[0]
        negres = self.votingclf_Neg[user].predict([text])[0]

        return {'username': user, 'Pos_vs_World': posres, 'Neg_vs_World': negres}

    @auth_basic(check_auth)
    def get_classification_by_file(self):

        """
        Ottiene la classificazione di un insieme di testi presenti all'interno di un file .csv
        formattato secondo lo stile "id","testo".
        Restituisce un file json
        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        file = request.files.get("file_test")

        filepath = "./persistence/" + user + "_file_test.csv"

        file.save(filepath, overwrite=True)

        test_data, test_id = GetData.GetTestData(filepath)

        posres = self.votingclf_Pos[user].predict([test_data])
        negres = self.votingclf_Neg[user].predict([test_data])

        responsejson = dict()

        responsejson['username'] = user

        for id_, pos, neg in zip(test_id, posres, negres):
            responsejson[id_]['Pos_vs_World'] = pos
            responsejson[id_]['Neg_vs_World'] = neg

        try:
            os.remove(filepath)
        except OSError:
            pass

        return responsejson

    @auth_basic(check_auth)
    def get_probability_text(self):

        """
       Ottiene le probabilità che un testo appartenga a determinate classi
       :return:
       """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        # curl -u user:password -F text=inserire_qui_il_testo http://indirizzo:porta/get/classification/text
        text = request.forms.get("text")

        le_posencoder = LabelEncoder()
        le_negencoder = LabelEncoder()
        le_posencoder.fit(['positive', 'rest_world'])
        le_negencoder.fit(['negative', 'rest_world'])

        if callable(getattr(self.votingclf_Pos[user], 'predict_proba', None)) and callable(
                getattr(self.votingclf_Neg[user], 'predict_proba', None)):
            posres = self.votingclf_Pos[user].predict_proba([text])[0]
            negres = self.votingclf_Neg[user].predict_proba([text])[0]

            resdict = dict()

            resdict['username'] = user

            for i, proba in enumerate(posres):
                resdict['Pos_vs_World'][le_posencoder.classes_[i]] = posres[i]

            for i, proba in enumerate(negres):
                resdict['Neg_vs_World'][le_negencoder.classes_[i]] = negres[i]

            return resdict
        else:
            return {'username': user, 'success': False,
                    'error': 'metodo predict_proba non disponibile per i classificatori scelti'}

    @auth_basic(check_auth)
    def get_probability_file(self):

        """
        Ottiene le probabilità di appartennza alle classi di un insieme di testi presenti all'interno di un file .csv
        formattato secondo lo stile "id","testo".
        Restituisce un file json
        :return:
        """

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        if callable(getattr(self.votingclf_Pos[user], 'predict_proba', None)) and callable(
                getattr(self.votingclf_Neg[user], 'predict_proba', None)):

            file = request.files.get("file_test")

            filepath = "./persistence/" + user + "_file_test.csv"

            file.save(filepath, overwrite=True)

            test_data, test_id = GetData.GetTestData(filepath)

            le_posencoder = LabelEncoder()
            le_negencoder = LabelEncoder()
            le_posencoder.fit(['positive', 'rest_world'])
            le_negencoder.fit(['negative', 'rest_world'])

            posres = self.votingclf_Pos[user].predict_proba([test_data])
            negres = self.votingclf_Neg[user].predict_proba([test_data])

            resdict = dict()

            resdict['username'] = user

            for row, id_ in enumerate(test_id):
                for i, proba in enumerate(posres):
                    resdict['Pos_vs_World'][id_][le_posencoder.classes_[i]] = posres[row][i]

            for row, id_ in enumerate(test_id):
                for i, proba in enumerate(negres):
                    resdict['Neg_vs_World'][id_][le_negencoder.classes_[i]] = negres[row][i]

            return resdict
        else:
            return {'username': user, 'success': False,
                    'error': 'metodo predict_proba non disponibile per i classificatori scelti'}

    @auth_basic(check_auth)
    def get_sentipolc14_probability(self):

        authstring = request.headers.get("Authorization")
        user = self.get_user_from_auth(authstring)

        if callable(getattr(self.votingclf_Pos[user], 'predict_proba', None)) and callable(
                getattr(self.votingclf_Neg[user], 'predict_proba', None)):

            parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
            filepath = os.path.join(parent_dir, "training_set_sentipolc16.csv")

            train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld(
                filepath)

            train_dataNeg, train_labelsNeg, test_dataNeg, test_labelsNeg, test_idNeg = GetData14.getNegVsWorld(
                filepath)

            le_posencoder = LabelEncoder()
            le_negencoder = LabelEncoder()
            le_posencoder.fit(['positive', 'rest_world'])
            le_negencoder.fit(['negative', 'rest_world'])

            predictionPos = self.votingclf_Pos[user].predict_proba(test_data)
            predictionNeg = self.votingclf_Neg[user].predict_proba(test_dataNeg)

            resdict = dict()

            resdict['username'] = user

            for row, id_ in enumerate(test_id):
                for i, proba in enumerate(predictionPos):
                    resdict['Pos_vs_World'][id_][le_posencoder.classes_[i]] = predictionPos[row][i]

            for row, id_ in enumerate(test_id):
                for i, proba in enumerate(predictionNeg):
                    resdict['Neg_vs_World'][id_][le_negencoder.classes_[i]] = predictionNeg[row][i]

            return resdict
        else:
            return {'username': user, 'success': False,
                    'error': 'metodo predict_proba non disponibile per i classificatori scelti'}





if __name__ == "__main__":
    webservice = SentimentWebService(args.host, args.port, server='mysslcherrypy')
    webservice.start()
