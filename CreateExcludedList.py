from argparse import ArgumentParser
import csv

argparser = ArgumentParser(description='')
argparser.add_argument('-r', dest='result_class1', action='store', default='overallRun.csv', help='CSV file of the run results of the first classifier (pos vs world)')
argparser.add_argument('-g', dest='gold_file', action='store', default="sentipolc14_gold_test.csv", help='gold standard annotation CSV file')
args = argparser.parse_args()

# read result data about the first classifier
resc1 = dict()
with open(args.result_class1) as f:
    for line in f:
        id, val, l1, l2, l3, l4 = map(lambda x: x[1:-1], line.rstrip().split(','))
        resc1[id] = {'val':val}

with open("exclude_tweets.txt", "w", encoding="utf-8") as exclude, open(args.gold_file) as f:
    for line in f:
        id, sub, pos, neg, iro, top = map(lambda x: x[1:-1], line.rstrip().split(','))
        if id not in resc1.keys():
            exclude.write(id + "\n")



