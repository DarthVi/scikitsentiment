from TweetAnalyzer import TweetAnalyzer
import TweetPurgeLib as tpl
import csv
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.grid_search import GridSearchCV
from sklearn import svm


def parseLine(line):
    line = tpl.purgeMentions(line)
    line = tpl.purgeRT(line)
    line = tpl.purgeHashtags(line)
    line = tpl.purgeLinks(line)
    line = tpl.purgeEmoji(line)
    line = tpl.purgeEscapedQuotes(line)
    line = tpl.purgeAtAmp(line)
    return line

if __name__ == "__main__":

    classes = ['positive', 'negative', 'mixed', 'neutral']

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []

    with open("sentipolc annotation gold v2.csv", encoding="utf-8") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            purged = []
            # il seguente blocco for serve a rendere gestibile l'encoding delle emoji scritte con caratteri non BMP
            for data in row:
                data = ''.join(c for c in data if c <= '\uFFFF')
                purged.append(data)

            purged[3] = parseLine(purged[3])

            if purged[4] == 'FALSE':
                if purged[8] == 'train':
                    train_data.append(purged[3])
                    train_labels.append('neutral')
                else:
                    test_data.append(purged[3])
                    test_labels.append('neutral')
            elif purged[5] == 'TRUE' and purged[6] == 'TRUE':
                if purged[8] == 'train':
                    train_data.append(purged[3])
                    train_labels.append('mixed')
                else:
                    test_data.append(purged[3])
                    test_labels.append('mixed')
            elif purged[5] == 'TRUE':
                if purged[8] == 'train':
                    train_data.append(purged[3])
                    train_labels.append('positive')
                else:
                    test_data.append(purged[3])
                    test_labels.append('positive')
            else:
                if purged[8] == 'train':
                    train_data.append(purged[3])
                    train_labels.append('negative')
                else:
                    test_data.append(purged[3])
                    test_labels.append('negative')

    text_clf = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer('italian'))),
                         ('tfidf', TfidfTransformer(use_idf=True)),
                         ('clf', SGDClassifier(loss='hinge', penalty='l2',
                                               alpha=1e-3, n_iter=5, random_state=42)),
                         ])

    parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
                  'tfidf__use_idf': (True, False),
                  'clf__alpha': (1e-2, 1e-3),
                  }

    # print(train_data[0])
    # print(train_labels[0])
    #
    # gs_clf = GridSearchCV(text_clf, parameters, n_jobs=-1)
    #
    # gs_clf = gs_clf.fit(train_data, train_labels)
    #
    # best_parameters, score, _ = max(gs_clf.grid_scores_, key=lambda x: x[1])
    # for param_name in sorted(parameters.keys()):
    #     print("%s: %r" % (param_name, best_parameters[param_name]))

    # print(score)

    text_clf.fit(train_data, train_labels)
    prediction = text_clf.predict(test_data)

    print("SGDClassifier")
    print(classification_report(test_labels, prediction))

    # text_clf = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer('italian'))),
    #                      ('tfidf', TfidfTransformer(use_idf=True)),
    #                      ('clf', svm.LinearSVC()),
    #                      ])
    #
    # text_clf.fit(train_data, train_labels)
    # prediction = text_clf.predict(test_data)
    #
    # print("MultinomialNB")
    # print(classification_report(test_labels, prediction))