from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
import GetData14
from EvalFileBuilder import Builder
import nltk
from sklearn import svm
from time import time
from customclassifiers.CustomAnalyzer import TweetAnalyzer

if __name__ == "__main__":

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    test_id = []

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)

    t0 = time()
    train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld("sentipolc annotation gold v2.csv")

    # text_clfPos = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(None, None))),
    #                      ('tfidf', TfidfTransformer(use_idf=True)),
    #                      ('clf', SGDClassifier(loss='hinge', penalty='l2',
    #                                            alpha=1e-3, n_iter=5, random_state=None)),
    #                      ])

    text_clfPos = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer('italian', stemmer=stemmer), max_df=0.5,
                                                     max_features=None, ngram_range=(1, 1))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=True, max_iter=1000)),
                            ])

    # parameters = {'vect__ngram_range': [(1, 1), (1, 2)],
    #               'tfidf__use_idf': (True, False),
    #               'clf__alpha': (1e-2, 1e-3),
    #               }

    text_clfPos.fit(train_data, train_labels)
    predictionPos = text_clfPos.predict(test_data)

    print("LinearSVC_PosVsWorld")
    print(classification_report(test_labels, predictionPos))

    train_dataNeg, train_labelsNeg, test_dataNeg, test_labelsNeg, test_idNeg = GetData14.getNegVsWorld("sentipolc annotation gold v2.csv")

    # text_clfNeg = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(None, None))),
    #                      ('tfidf', TfidfTransformer(use_idf=True)),
    #                      ('clf', SGDClassifier(loss='hinge', penalty='l2',
    #                                            alpha=1e-3, n_iter=5, random_state=None)),
    #                      ])

    text_clfNeg = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer('italian', stemmer=stemmer),
                                                     max_df=0.5, max_features=None, ngram_range=(1, 1))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=True, max_iter=1000)),
                            ])

    text_clfNeg.fit(train_dataNeg, train_labelsNeg)
    predictionNeg = text_clfNeg.predict(test_dataNeg)

    print("LinearSVC_NegVsWorld")
    print(classification_report(test_labelsNeg, predictionNeg))
    print("done in %0.3fs" % (time() - t0))

    evalbuilder = Builder("overallRun.csv")

    evalbuilder.getResFile(test_id, predictionPos, test_idNeg, predictionNeg)