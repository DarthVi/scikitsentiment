import nltk
import string
from nltk.corpus import stopwords
import pprint
import treetaggerwrapper
from EmojiParser import EmojiParser
from TreeTagger import TreeTagger

from collections import Counter

# Create a dictionary using a comprehension - this maps every character from
# string.punctuation to None. Initialize a translation object from it.
translator = str.maketrans({key: None for key in string.punctuation})

text = "Ciao, Mondo. Il sole splende fuori dalla finestra."

lowers = text.lower()
#no_punctuation = lowers.translate(lowers.maketrans("",""), string.punctuation)
no_punctuation = lowers.translate(translator)

tokens = nltk.word_tokenize(no_punctuation)

filtered = [w for w in tokens if not w in stopwords.words('italian')]

print(filtered)

tagger = treetaggerwrapper.TreeTagger(TAGLANG='it', TAGPARFILE='italian_utf8.par')

tags = tagger.tag_text(tokens)

pprint.pprint(tags)

text2 = "Allora andiamo a mangiare! #emoSMILE#"

tags2 = tagger.tag_text(text2)

pprint.pprint(tags2)

list = [s.split('\t') for s in tags2]

listConcat = [s[2] + '_' + s[1] for s in list]

pprint.pprint(list)

pprint.pprint(listConcat)

emoparser = EmojiParser()

print(emoparser.parseLine("Proviamo :) il parsing delle emoticon D: looooool"))

customTagger = TreeTagger('italian', nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True))

pprint.pprint(customTagger.tag_text("Proviamo :) il parsing delle emoticon D:"))

print(stopwords.words('italian'))