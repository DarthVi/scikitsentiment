#!/usr/bin/env python
# sentipolc14_evaluate_py3.py
# evaluation script for the SENTIPOLC shared task


from argparse import ArgumentParser

argparser = ArgumentParser(description='')
argparser.add_argument('-r', dest='result_file', action='store', default="overallRun.csv", help='CSV file of the run results')
argparser.add_argument('-g', dest='gold_file', action='store', default="sentipolc14_gold_test.csv", help='gold standard annotation CSV file')
argparser.add_argument('-e', dest='exclude_file', action='store', default="exclude_tweets.txt", help='list of tweets ID to exclude from the evaluation')
args = argparser.parse_args()

# read gold standard and populate the count matrix
gold = dict()
gold_counts =  {'sub':{'0':0,'1':0},
                'pos':{'0':0,'1':0},
                'neg':{'0':0,'1':0},
                'iro':{'0':0,'1':0}}

with open(args.gold_file) as f:
    for line in f:
        id, sub, pos, neg, iro, top = map(lambda x: x[1:-1], line.rstrip().split(','))
        gold[id] = {'sub':sub, 'pos':pos, 'neg':neg, 'iro':iro}
        gold_counts['sub'][sub]+=1
        gold_counts['pos'][pos]+=1
        gold_counts['neg'][neg]+=1
        gold_counts['iro'][iro]+=1

# read tweets IDs to exclude
with open(args.exclude_file) as f:
    #exclude = map(lambda x:x.rstrip(), f.readlines())
    exclude = list()
    for line in f:
        exclude.append(line.rstrip())


# read result data
result = dict()
with open(args.result_file) as f:
    for line in f:
        id, sub, pos, neg, iro, top = map(lambda x: x[1:-1], line.rstrip().split(','))
        if not id in exclude:
            result[id]= {'sub':sub, 'pos':pos, 'neg':neg, 'iro':iro}            

# evaluation: single classes
for task in ['sub', 'pos', 'neg', 'iro']:
    # table header
    print("\ntask: {}".format(task))
    print("prec. 0\trec. 0\tF-sc. 0\tprec. 1\trec. 1\tF-sc. 1\tF-sc.")
    correct =  {'0':0,'1':0}
    assigned = {'0':0,'1':0}
    precision ={'0':0.0,'1':0.0}
    recall =   {'0':0.0,'1':0.0}
    fscore =   {'0':0.0,'1':0.0}
   
    # count the labels
    for id, gold_labels in iter(gold.items()):
        if not id in exclude: #ignore not unavailable tweets
            if (not id in result) or result[id][task]=='':
                pass
            else:
                assigned[result[id][task]] += 1                    
                if gold_labels[task]==result[id][task]:
                    correct[result[id][task]] += 1

    # compute precision, recall and F-score
    for label in ['0','1']:
        try:
            precision[label] = float(correct[label])/float(assigned[label])
            recall[label] = float(correct[label])/float(gold_counts[task][label])
            fscore[label] = (2.0 * precision[label] * recall[label]) / (precision[label] + recall[label])
        except:
            # if a team doesn't participate in a task it gets default 0 F-score
            fscore[label] = 0.0
            
    # write down the table
    print ("{0:.4f}\t{1:.4f}\t{2:.4f}\t{3:.4f}\t{4:.4f}\t{5:.4f}\t{6:.4f}".format(
            precision['0'], recall['0'], fscore['0'], 
            precision['1'], recall['1'], fscore['1'],
            (fscore['0'] + fscore['1'])/2.0))
            
            
# polarity evaluation needs a further step
print ("\ntask: polarity")
print ("Combined F-score")
correct =  {'pos':{'0':0,'1':0}, 'neg':{'0':0,'1':0}}
assigned = {'pos':{'0':0,'1':0}, 'neg':{'0':0,'1':0}}
precision ={'pos':{'0':0.0,'1':0.0}, 'neg':{'0':0.0,'1':0.0}}
recall =   {'pos':{'0':0.0,'1':0.0}, 'neg':{'0':0.0,'1':0.0}}
fscore =   {'pos':{'0':0.0,'1':0.0}, 'neg':{'0':0.0,'1':0.0}}

# count the labels
for id, gold_labels in iter(gold.items()):
    if not id in exclude: #ignore not unavailable tweets
        for cl in ['pos','neg']:
            if (not id in result) or result[id][cl]=='':
                pass
            else:
                assigned[cl][result[id][cl]] += 1
                if gold_labels[cl]==result[id][cl]:
                    correct[cl][result[id][cl]] += 1
            
# compute precision, recall and F-score
for cl in ['pos','neg']:
    for label in ['0','1']:
        try:
            precision[cl][label] = float(correct[cl][label])/float(assigned[cl][label])
            recall[cl][label] = float(correct[cl][label])/float(gold_counts[cl][label])
            fscore[cl][label] = float(2.0 * precision[cl][label] * recall[cl][label]) / float(precision[cl][label] + recall[cl][label])
        except:
            fscore[cl][label] = 0.0

fscore_pos = (fscore['pos']['0'] + fscore['pos']['1'] ) / 2.0
fscore_neg = (fscore['neg']['0'] + fscore['neg']['1'] ) / 2.0

# write down the table
print ("{0:.4f}".format((fscore_pos + fscore_neg)/2.0))

