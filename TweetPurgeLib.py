'''
    Libreria di funzioni usate per eliminare dai tweet il carattere '@'
    nelle menzioni, '#' negli hashtag e i link.
    Il tutto viene effettuato tramite l'uso di regex e importando il modulo re.
'''
import re

def purgeMentions(str):
    '''
    Elimina le menzioni (@username) e restituisce la stringa nuova.
    '''

    #@ - match del carattere '@'
    #(\w+\b) - match di uno o più caratteri qualsiasi e di una word boundary
    pattern = re.compile(r'@(\w+\b)')
    return re.sub(pattern, "", str)

def purgeLinks(str):
    '''
    Elimina i link dalla stringa e ne restituisce la versione senza link
    '''

    #questa regex è presa da http://stackoverflow.com/a/6883094/1262118
    pattern = re.compile(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
    ns = re.sub(pattern, "", str)
    pattern = re.compile(r'(http:/…)')
    return re.sub(pattern, "", ns)

def purgeHashtags(str):
    '''
    Elimina '#' dagli hashtag e restituisce la stringa nuova.
    '''
    
    ## - match del carattere '#'
    #(\w+\b) - match di uno o più caratteri qualsiasi e di una word boundary
    pattern = re.compile(r'#(\w+\b)')
    return re.sub(pattern, r'\g<1>', str)

def purgeRT(str):
    '''
    Elimina RT (retweet) e restituisce la stringa nuova
    :param str:
    :return:
    '''
    pattern = re.compile(r'(RT :)')
    ns = re.sub(pattern, "", str)
    pattern = re.compile(r'(^rt\b)')
    return re.sub(pattern, "", ns)

def purgeEmoji(str):
    '''
    Trasforma i codici html di alcuni caratteri (tipo &lt;) con i corrispettivi
    unicode. Il tutto viene effettuato perché tali caratteri vengono spesso usati
    per le emoticon.
    '''
    pattern = re.compile(r'&lt;')
    ns = re.sub(pattern, r'<', str)
    pattern = re.compile(r'&gt;')
    ns = re.sub(pattern, r'>', ns)
    return ns

# def purgeAtAmp(str):
#     '''
#     Sostituisce "&" con "&amp;" in modo da far funzionare il SAXParser
#     :param str:
#     :return:
#     '''
#     #questa regex fa uso del negative lookahead: specifica un gruppo che deve essere controllato
#     #dopo l'espressione principale (in questo caso "&"); se il match avviene, il risultato viene
#     #scartato
#     pattern = re.compile(r'&((?!amp;)(?!lt;)(?!gt;))')
#     ns = re.sub(pattern, r'&amp;', str)
#     return ns

def purgeAtAmp(str):
    '''
    Gestisce il codice &amp sostituendolo con il carattere ad esso correlato
    :param str:
    :return:
    '''
    pattern = re.compile(r'&amp;#039;')
    ns = re.sub(pattern, "\'", str)
    pattern = re.compile(r'\s&amp;\s')
    ns = re.sub(pattern, r' e ', ns)
    pattern = re.compile(r'&amp;')
    ns = re.sub(pattern, r' e ', ns)
    return ns

def purgeEscapedQuotes(str):
    '''
    Gestisce la sequenza \""
    :param str:
    :return:
    '''
    pattern = re.compile(r'\\""')
    ns = re.sub(pattern, r'""', str)
    return ns
