from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
import GetData14
from EvalFileBuilder import Builder
import nltk
from sklearn.linear_model import SGDClassifier

if __name__ == "__main__":

    classes = ['positive', 'rest_world']

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    test_id = []

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)

    train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld("sentipolc annotation gold v2.csv")

    text_clfPos = Pipeline([('vect', CountVectorizer('word', max_df=1.0, max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2', sublinear_tf=True)),
                         ('clf', SGDClassifier()),
                         ])

    text_clfPos.fit(train_data, train_labels)
    predictionPos = text_clfPos.predict(test_data)

    print("SGDClassifier_PosVsWorld")
    print(classification_report(test_labels, predictionPos))

    train_dataNeg, train_labelsNeg, test_dataNeg, test_labelsNeg, test_idNeg = GetData14.getNegVsWorld("sentipolc annotation gold v2.csv")

    text_clfNeg = Pipeline([('vect', CountVectorizer('word', max_df=1.0, max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                         ('clf', SGDClassifier()),
                         ])

    text_clfNeg.fit(train_dataNeg, train_labelsNeg)
    predictionNeg = text_clfNeg.predict(test_dataNeg)

    print("SGDClassifier_NegVsWorld")
    print(classification_report(test_labelsNeg, predictionNeg))

    evalbuilder = Builder("overallRun.csv")

    evalbuilder.getResFile(test_id, predictionPos, test_idNeg, predictionNeg)