import csv
import TweetPurgeLib as tpl

def parseLine(line):
    line = tpl.purgeMentions(line)
    line = tpl.purgeRT(line)
    line = tpl.purgeHashtags(line)
    line = tpl.purgeLinks(line)
    line = tpl.purgeEmoji(line)
    line = tpl.purgeEscapedQuotes(line)
    line = tpl.purgeAtAmp(line)
    return line

def getTestingData(path):

    """
    Ottiene i dati da un file .csv. Questa funzione permette di ottenere i dati di testing di sentipolc 2016
    :param path: file .csv che contiene i tweet
    :return: 3 liste, ossia i dati di testing
    """

    test_data = []
    test_id = []

    with open(path, encoding="utf-8") as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)
        for row in reader:
            purged = []
            # il seguente blocco for serve a rendere gestibile l'encoding delle emoji scritte con caratteri non BMP
            for data in row:
                data = ''.join(c if c <= '\uFFFF' else hex(ord(c)) for c in data)
                purged.append(data)

            purged[8] = parseLine(purged[8])

            test_data.append(purged[8])

            test_id.append(purged[0])

        return test_data, test_id
