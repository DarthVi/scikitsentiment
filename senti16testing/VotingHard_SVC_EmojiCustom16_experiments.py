from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from senti16testing.EvalFileBuilder16 import Builder
from sklearn.metrics import classification_report
from sklearn.ensemble import VotingClassifier
from sklearn.cross_validation import KFold
from senti16testing.GetTestingData16 import getTestingData
from customclassifiers.EmojiNaiveClassifier import EmojiClassifier
from customclassifiers.EmojiCustomClassifier import EmojiCustomClassifier
from customclassifiers.CustomAnalyzer import TweetAnalyzer
from nltk.corpus import stopwords
import GetData16
import nltk
from sklearn import svm
from time import time
from customclassifiers.Doc2VecUtils import Doc2VecModel, Doc2VecTransformer
import numpy as np

if __name__ == "__main__":

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)
    stopwordsList = stopwords.words("italian")
    spwords = ['è', 'e', 'il', 'di', 'a', 'da', 'dello', 'del', 'un']

    t0 = time()
    dataPos, labelPos, idPos = GetData16.getPosVsWorld("training_set_sentipolc16.csv")
    testData, testId = getTestingData("testset_sentipolc16.csv")

    text_clfPos = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(stopwords=stopwordsList, stemmer=stemmer),
                                                       max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=False, max_iter=1000)),
                            ])

    emo_clfPos = EmojiCustomClassifier()

    votingclf_Pos = VotingClassifier(estimators=[('text_clfPos', text_clfPos), ('emo_clfPos', emo_clfPos)],
                                     voting='hard')


    votingclf_Pos.fit(dataPos, labelPos)
    predictionPos = votingclf_Pos.predict(testData)

    dataNeg, labelNeg, idNeg = GetData16.getNegVsWorld("training_set_sentipolc16.csv")

    text_clfNeg = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(stopwords=stopwordsList, stemmer=stemmer),
                                                       max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=True, max_iter=1000)),
                            ])

    emo_clfNeg = EmojiCustomClassifier()


    votingclf_Neg = VotingClassifier(estimators=[('text_clfNeg', text_clfNeg), ('emo_clfNeg', emo_clfNeg)], voting='hard')


    votingclf_Neg.fit(dataNeg, labelNeg)
    predictionNeg = votingclf_Neg.predict(testData)

    print("done in %0.3fs" % (time() - t0))

    evalbuilder = Builder("overallRun16.csv")

    evalbuilder.getResFile(testId, predictionPos, predictionNeg)