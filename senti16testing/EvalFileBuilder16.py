import csv

class Builder:

    def __init__(self, path):
        self.path = path

    def getResFile(self, test_id, test_labelsPos, test_labelsNeg):

        with open(self.path, "w", newline='', encoding="utf-8") as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
            for i, id in enumerate(test_id):
                runlist = list()
                runlist.append(id)
                runlist.append('')

                posVal = None
                negVal = None

                if test_labelsPos[i] == 'positive':
                    posVal = '1'
                else:
                    posVal = '0'


                if test_labelsNeg[i] == 'negative':
                    negVal = '1'
                else:
                    negVal = '0'

                runlist.append(posVal)
                runlist.append(negVal)
                runlist.append('')
                runlist.append('0')
                spamwriter.writerow(runlist)