def getData(filepath, label, encoding):
    """
        Legge una riga per volta, scartando quelle in cui c'è solo la newline.
        Riesce a rilevare i post coposta da più righe aggregandone il contenuto in una unica stringa; ogni post/stringa
        viene poi salvata nella lista e la label corrispondente viene memorizzata nell'altra lista.
        La label passata come funzione serve non solo a salvare la classe di ogni post, ma anche a rilevera quando
        una riga contiene nel file il post intero o quando quest'ultimo è suddiviso in più righe.

        :param filepath:
        :param label:
        :param encoding:
        :return:
        """

    reslist = list()
    labels = list()

    with open(filepath, "r", encoding=encoding) as file:


        line = file.readline()
        while line:

            line = line.strip()
            lastString = ""
            extractedPost = line[:len(line) - 6]
            lastString = line[len(line) - 5:]

            while (lastString != label):
                line = file.readline().strip()
                lastString = line[len(line) - 5:]
                extractedPost += ' ' + line[:len(line) - 6]

            reslist.append(extractedPost)
            labels.append(label[1:-1])
            line = file.readline()  # blank line
            line = file.readline()

    return reslist, labels

def getDataWithLabel(filepath, label, encoding):

    """
    Legge una riga per volta, scartando quelle in cui c'è solo la newline.
    Riesce a rilevare i post coposta da più righe aggregandone il contenuto in una unica stringa; ogni post/stringa
    viene poi salvata nella lista e la label corrispondente viene memorizzata nell'altra lista.
    La label non viene ricavata dal file, ma come argomento passato alla funzione.

    :param filepath:
    :param label:
    :param encoding:
    :return:
    """

    reslist = list()
    labels = list()

    with open(filepath, "r", encoding=encoding) as file:

        for line in file:

            exctractedPost = ""

            while line != '\n' and line:
                line = line.strip()
                exctractedPost += line + ' '
                line = file.readline()

            reslist.append(exctractedPost)
            labels.append(label)

    return reslist, labels