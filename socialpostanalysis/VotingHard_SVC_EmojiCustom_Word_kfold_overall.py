from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.ensemble import VotingClassifier
from sklearn.cross_validation import KFold
from customclassifiers.EmojiNaiveClassifier import EmojiClassifier
from customclassifiers.EmojiCustomClassifier import EmojiCustomClassifier
from nltk.corpus import stopwords
from socialpostanalysis.extractutils import getDataWithLabel
from EvalFileBuilder import Builder
import numpy as np
import nltk
from sklearn import svm
from time import time
from customclassifiers.Doc2VecUtils import Doc2VecModel, Doc2VecTransformer
from customclassifiers.CustomAnalyzer import TweetAnalyzer

if __name__ == "__main__":

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)
    spwords = ['è', 'e', 'il', 'di', 'a', 'da', 'dello', 'del', 'un']

    t0 = time()
    train_data, train_labels = getDataWithLabel("Positivi.txt", "positive", "utf-8")
    train_dataNeg, train_labelsNeg = getDataWithLabel("Negativi.txt", "negative", "utf-8")

    train_data += train_dataNeg
    train_labels += train_labelsNeg

    train_data = np.asarray(train_data)
    train_labels = np.asarray(train_labels)

    print(len(train_data))

    text_clfPos = Pipeline([('vect', CountVectorizer(analyzer="word", max_df=0.5,
                                                     max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=False, max_iter=1000)),
                            ])
    emo_clfPos = EmojiCustomClassifier()

    # d2vPos = Doc2VecModel()
    # d2vPos.fit(train_data)
    # modelPos = d2vPos.get_model()
    #
    # d2v_clfPos = Pipeline([('vect', Doc2VecTransformer(model=modelPos)),
    #                         ('clf', svm.LinearSVC()),
    #                         ])

    votingclf_Pos = VotingClassifier(estimators=[('text_clfPos', text_clfPos), ('emo_clfPos', emo_clfPos)], voting='hard')

    kfPos = KFold(n=len(train_data), n_folds=3, shuffle=True)

    i = 1
    for train_index, test_index in kfPos:
        X_train, X_test = train_data[train_index], train_data[test_index]
        y_train, y_test = train_labels[train_index], train_labels[test_index]

        votingclf_Pos.fit(X_train, y_train)
        predictionPos = votingclf_Pos.predict(X_test)

        print("VotingClassifier SVC EmojiCustom Word analyzer fold i: ", i)
        print(classification_report(y_test, predictionPos))
        print("Classification accuracy: ", votingclf_Pos.score(X_test, y_test))
        i += 1