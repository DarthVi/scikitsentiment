from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report, f1_score
from sklearn.ensemble import VotingClassifier
from sklearn.cross_validation import KFold
from customclassifiers.EmojiNaiveClassifier import EmojiClassifier
from customclassifiers.EmojiCustomClassifier import EmojiCustomClassifier
from nltk.corpus import stopwords
from socialpostanalysis.extractutils import getDataWithLabel
from EvalFileBuilder import Builder
import numpy as np
import nltk
from sklearn import svm
from time import time
from customclassifiers.Doc2VecUtils import Doc2VecModel, Doc2VecTransformer
from TweetAnalyzer import TweetAnalyzer

if __name__ == "__main__":

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)
    spwords = ['è', 'e', 'il', 'di', 'a', 'da', 'dello', 'del', 'un']

    t0 = time()
    train_data, train_labels = getDataWithLabel("Positivi.txt", "positive", "utf-8")
    train_dataNeg, train_labelsNeg = getDataWithLabel("Negativi.txt", "negative", "utf-8")

    train_data += train_dataNeg
    train_labels += train_labelsNeg

    train_data = np.asarray(train_data)
    train_labels = np.asarray(train_labels)

    text_clfPos = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(), max_df=0.5,
                                                     max_features=None, ngram_range=(1, 2))),
                            ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
                            ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=True, max_iter=1000)),
                            ])
    # emo_clfPos = EmojiCustomClassifier()

    # d2vPos = Doc2VecModel()
    # d2vPos.fit(train_data)
    # modelPos = d2vPos.get_model()
    #
    # d2v_clfPos = Pipeline([('vect', Doc2VecTransformer(model=modelPos)),
    #                         ('clf', svm.LinearSVC()),
    #                         ])

    # votingclf_Pos = VotingClassifier(estimators=[('text_clfPos', text_clfPos), ('emo_clfPos', emo_clfPos)], voting='hard')

    kfPos = KFold(n=len(train_data), n_folds=10, shuffle=True)

    i = 1
    f_score = 0.0
    for train_index, test_index in kfPos:
        X_train, X_test = train_data[train_index], train_data[test_index]
        y_train, y_test = train_labels[train_index], train_labels[test_index]

        text_clfPos.fit(X_train, y_train)
        predictionPos = text_clfPos.predict(X_test)

        print("SVC TweetAnalyzer analyzer fold i: ", i)
        print(classification_report(y_test, predictionPos))
        print("Classification accuracy: ", text_clfPos.score(X_test, y_test))
        temp_f_measure = f1_score(y_test, predictionPos, pos_label=None, average='macro')
        f_score += temp_f_measure
        print("Classification f1-score: ", temp_f_measure)
        i += 1

    print("\n\n")

    print("Positive vs Negative f1-score macro: ", f_score / (i - 1))

    print("\n\n")