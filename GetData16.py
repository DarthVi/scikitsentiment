import csv
import TweetPurgeLib as tpl

def parseLine(line):
    line = tpl.purgeMentions(line)
    line = tpl.purgeRT(line)
    line = tpl.purgeHashtags(line)
    line = tpl.purgeLinks(line)
    line = tpl.purgeEmoji(line)
    line = tpl.purgeEscapedQuotes(line)
    line = tpl.purgeAtAmp(line)
    return line

def getPosVsWorld(path):

    """
    Ottiene i dati da un file .csv. Questa funzione permette di suddividere i dati in due classi, positivi e resto del
    mondo (negativi and neutral)
    :param path: file .csv che contiene i tweet
    :return: 3 liste, ossia i dati di training, le rispettive classi associate, lista di id dei dati di testing
    """

    train_data = []
    train_labels = []
    test_id = []

    with open(path, encoding="utf-8") as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)
        for row in reader:
            purged = []
            # il seguente blocco for serve a rendere gestibile l'encoding delle emoji scritte con caratteri non BMP
            for data in row:
                data = ''.join(c if c <= '\uFFFF' else hex(ord(c)) for c in data)
                purged.append(data)

            purged[8] = parseLine(purged[8])

            train_data.append(purged[8])

            if purged[1] == '1' and purged[2] == '1':
                train_labels.append('positive')
            else:
                train_labels.append('rest_world')

            test_id.append(purged[0])

        return train_data, train_labels, test_id


def getNegVsWorld(path):

    """
    Ottiene i dati da un file .csv. Questa funzione permette di suddividere i dati in due classi, negativi e resto del
    mondo (positivi and neutral)
    :param path: file .csv che contiene i tweet
    :return: 3 liste, ossia i dati di training, le rispettive classi associate, lista di id dei dati di testing
    """

    train_data = []
    train_labels = []
    test_id = []

    with open(path, encoding="utf-8") as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)
        for row in reader:
            purged = []
            # il seguente blocco for serve a rendere gestibile l'encoding delle emoji scritte con caratteri non BMP
            for data in row:
                data = ''.join(c if c <= '\uFFFF' else hex(ord(c)) for c in data)
                purged.append(data)

            purged[8] = parseLine(purged[8])

            train_data.append(purged[8])

            if purged[1]=='1' and purged[3]=='1':
                train_labels.append('negative')
            else:
                train_labels.append('rest_world')

            test_id.append(purged[0])

        return train_data, train_labels, test_id
