import treetaggerwrapper
import nltk
from EmojiParser import EmojiParser
import re
from nltk.corpus import stopwords


class TreeTagger:
    """
    Wrapper del TreeTagger. Viene definito per poter aggiungere il supporto alle emoticon
    durante l'operazione di pos-tagging
    """

    # custom translator usato per eliminare i segni di punteggiatura
    translator = str.maketrans({key: None for key in ',.;'})

    def __init__(self, stopwords=None, stemmer=None):
        self.treetagger = treetaggerwrapper.TreeTagger(TAGLANG='it', TAGPARFILE='italian.par')
        self.stopwords = stopwords
        self.stemmer = stemmer
        self.poslist = ['NOUN', 'WH', 'CLI', 'ADV', 'NEG', 'CON', 'CHE', 'DET', 'NPR', 'PRE', 'ART', 'INT', 'ADJ', 'VER', 'PRO', 'AUX']

    def tag_text(self, text):
        """

        :param text: stringa da cui ottenere gli elementi parole_POSTAG
        :return: lista di elementi del tipo parola_POSTAG
        """

        # parsing delle emoticon
        emoparser = EmojiParser()
        text = emoparser.parseLine(text)

        # eliminazione dei segni di punteggiatura
        no_punctuation = text.translate(self.translator)

        if self.stopwords is not None:
            tknz = nltk.tokenize.TweetTokenizer()
            #tokens = nltk.word_tokenize(no_punctuation)
            tokens = tknz.tokenize(no_punctuation)
            filtered = [w for w in tokens if not w in stopwords.words(self.stopwords)]
            no_punctuation = ' '.join(filtered)
            #print(no_punctuation)

        tags = self.treetagger.tag_text(no_punctuation)
        list = [s.split('\t') for s in tags]

        if self.stemmer is not None:
            listconcat = [self.stemmer.stem(s[0]) + '_' + s[1] for s in list if len(s) == 3 and s[1].split(":")[0] in self.poslist]
        else:
            listconcat = [s[2] + '_' + s[1] for s in list if len(s) == 3 and s[1].split(":")[0] in self.poslist]

        # itera nella lista composta da parole_POSTAG
        # se uno degli elementi è del tipo #emo#_POSTAG
        # allora sostituisce tale elemento con #emo#_EMO
        pattern = re.compile(r'#\w*')
        for n, entry in enumerate(listconcat):
            if re.match(pattern, entry) is not None:
                listconcat[n] = listconcat[n].split('_')[0] + '_EMO'

        return listconcat

    def __getstate__(self):
        state = self.__dict__.copy()
        del state['treetagger']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self.treetagger = treetaggerwrapper.TreeTagger(TAGLANG='it', TAGPARFILE='italian.par')