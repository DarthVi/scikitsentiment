from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.ensemble import VotingClassifier
from customclassifiers.EmojiNaiveClassifier import EmojiClassifier
from customclassifiers.EmojiCustomClassifier import EmojiCustomClassifier
from customclassifiers.CustomAnalyzer import TweetAnalyzer
from nltk.corpus import stopwords
import GetData14
from EvalFileBuilder import Builder
import nltk
from sklearn.naive_bayes import MultinomialNB
from time import time
from customclassifiers.Doc2VecUtils import Doc2VecModel, Doc2VecTransformer
from customclassifiers.CustomAnalyzer import TweetAnalyzer
from sklearn import svm

if __name__ == "__main__":

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    test_id = []

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)
    spwords = ['è', 'e', 'il', 'di', 'a', 'da', 'dello', 'del', 'un']

    t0 = time()
    train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld("sentipolc annotation gold v2.csv")

    text_clfPos = Pipeline([('vect', CountVectorizer(analyzer=TweetAnalyzer(stemmer=stemmer), max_df=0.5, max_features=None, ngram_range=(1, 1))),
                            ('clf', MultinomialNB(alpha=1.0, fit_prior=True)),
                            ])
    emo_clfPos = EmojiCustomClassifier(use_tfidf=False)

    # d2vPos = Doc2VecModel()
    # d2vPos.fit(train_data)
    # modelPos = d2vPos.get_model()
    #
    # d2v_clfPos = Pipeline([('vect', Doc2VecTransformer(model=modelPos)),
    #                         ('clf', svm.LinearSVC()),
    #                         ])

    votingclf_Pos = VotingClassifier(estimators=[('text_clfPos', text_clfPos), ('emo_clfPos', emo_clfPos)], voting='hard')

    votingclf_Pos.fit(train_data, train_labels)
    predictionPos = votingclf_Pos.predict(test_data)

    print("VotingClassifier SVC_EmojiClassifier_PosVsWorld")
    print(classification_report(test_labels, predictionPos))

    train_dataNeg, train_labelsNeg, test_dataNeg, test_labelsNeg, test_idNeg = GetData14.getNegVsWorld("sentipolc annotation gold v2.csv")

    text_clfNeg = Pipeline(
        [('vect', CountVectorizer(analyzer='word', max_df=0.5, max_features=None, ngram_range=(1, 2))),
         ('tfidf', TfidfTransformer(use_idf=True, norm='l2')),
         ('clf', svm.LinearSVC(class_weight='balanced', dual=True, fit_intercept=True, max_iter=1000)),
         ])

    emo_clfNeg = EmojiCustomClassifier()
    # d2vNeg = Doc2VecModel()
    # d2vNeg.fit(train_dataNeg)
    # modelNeg = d2vNeg.get_model()
    #
    # d2v_clfNeg = Pipeline([('vect', Doc2VecTransformer(model=modelNeg)),
    #                         ('clf', svm.LinearSVC()),
    #                         ])

    votingclf_Neg = VotingClassifier(estimators=[('text_clfNeg', text_clfNeg), ('emo_clfNeg', emo_clfNeg)], voting='hard')
    votingclf_Neg.fit(train_dataNeg, train_labelsNeg)
    predictionNeg = votingclf_Neg.predict(test_dataNeg)

    print("VotingClassifier SVC_EmojiClassifier_NegVsWorld")
    print(classification_report(test_labelsNeg, predictionNeg))
    print("done in %0.3fs" % (time() - t0))
    print("Classification accuracy of pos and neg: ", votingclf_Pos.score(test_data, test_labels),
          votingclf_Neg.score(test_dataNeg, test_labelsNeg))

    evalbuilder = Builder("overallRun.csv")

    evalbuilder.getResFile(test_id, predictionPos, test_idNeg, predictionNeg)

    #print(text_clfPos.named_steps['tfidf'].get_feature_names())