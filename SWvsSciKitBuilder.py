import csv


class Builder:
    def __init__(self, path, exclude):
        self.path = path
        self.exclude = exclude
        self.excludedlist = []

    def getExcludedRes(self, test_idPos, test_labelsPos, test_idNeg, test_labelsNeg):

        for id in test_idPos:
            if id not in test_idNeg:
                self.excludedlist.append(id)

        for id in test_idNeg:
            if id not in test_idPos:
                self.excludedlist.append(id)

    def getFistEntry(self, id, list):

        for i, item in enumerate(list):
            if item == id:
                return i

        return None

    def getResFile(self, test_idPos, test_labelsPos, test_idNeg, test_labelsNeg):

        self.getExcludedRes(test_idPos, test_labelsPos, test_idNeg, test_labelsNeg)

        with open(self.path, "w", newline='', encoding="utf-8") as csvfile, open(self.exclude, "w", encoding="utf-8") as excludepath:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
            for i, id in enumerate(test_idPos):
                if id not in self.excludedlist:
                    runlist = list()
                    runlist.append(id)
                    runlist.append('')

                    posVal = None
                    negVal = None

                    if test_labelsPos[i] == 'positive':
                        posVal = '1'
                    else:
                        posVal = '0'

                    negindex = self.getFistEntry(id, test_idNeg)

                    if negindex is not None and test_labelsNeg[negindex] == 'negative':
                        negVal = '1'
                    else:
                        negVal = '0'

                    runlist.append(posVal)
                    runlist.append(negVal)
                    runlist.append('')
                    runlist.append('0')
                    spamwriter.writerow(runlist)

                else:
                    pass
                    #excludepath.write(id + "\n")