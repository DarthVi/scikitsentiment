from customclassifiers.CustomAnalyzer import TweetAnalyzer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
import GetData16
import nltk
from sklearn.grid_search import GridSearchCV
from sklearn import svm
from pprint import pprint
from time import time
import os

if __name__ == "__main__":

    parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    filepath = os.path.join(parent_dir, "training_set_sentipolc16.csv")

    stemmer_nostopword = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)
    stemmer_yesstopword = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=False)

    dataPos, labelPos, idPos = GetData16.getPosVsWorld(filepath)

    text_clfPos = Pipeline([('vect', CountVectorizer()),
                            ('tfidf', TfidfTransformer()),
                            ('clf', svm.LinearSVC()),
                            ])

    parameters = {
        'vect__analyzer': (TweetAnalyzer(None, stemmer_nostopword), TweetAnalyzer(None, stemmer_yesstopword),
                           TweetAnalyzer('italian', stemmer_nostopword), TweetAnalyzer('italian', None),
                           TweetAnalyzer(), 'word'),
        'vect__max_df': (0.5, 0.75, 1.0),
        'vect__max_features': (None, 5000, 10000, 50000),
        'vect__ngram_range': ((1, 1), (1, 2)),  # unigrams or bigrams
        'tfidf__use_idf': (True, False),
        'tfidf__norm': ('l1', 'l2'),
        'clf__dual': (True, False),
        #'clf__penalty': ('l1', 'l2'),
        'clf__fit_intercept': (True, False),
        #'clf__loss': ('hinge', 'squared_hinge'),
        'clf__max_iter': (1000, 5000),
        'clf__class_weight': ('balanced', {'positive': 5, 'rest_world': 1})
    }

    grid_search = GridSearchCV(text_clfPos, parameters, n_jobs=-1, verbose=1)

    print("Performing grid search...")
    print("pipeline:", [name for name, _ in text_clfPos.steps])
    print("parameters:")
    pprint(parameters)
    t0 = time()
    grid_search.fit(dataPos, labelPos)
    print("done in %0.3fs" % (time() - t0))
    print()

    print("Best score: %0.3f" % grid_search.best_score_)
    print("Best parameters set:")
    best_parameters = grid_search.best_estimator_.get_params()
    for param_name in sorted(parameters.keys()):
        print("\t%s: %r" % (param_name, best_parameters[param_name]))