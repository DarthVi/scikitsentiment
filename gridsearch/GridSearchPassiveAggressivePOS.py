from TweetAnalyzer import TweetAnalyzer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
import GetData14
import nltk
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import PassiveAggressiveClassifier
from pprint import pprint
from time import time
import os

if __name__ == "__main__":

    parent_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    filepath = os.path.join(parent_dir, "sentipolc annotation gold v2.csv")

    train_data = []
    train_labels = []
    test_data = []
    test_labels = []
    test_id = []

    stemmer = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)

    train_data, train_labels, test_data, test_labels, test_id = GetData14.getPosVsWorld(filepath)

    text_clfPos = Pipeline([('vect', CountVectorizer()),
                            ('tfidf', TfidfTransformer()),
                            ('clf', PassiveAggressiveClassifier()),
                            ])

    parameters = {
        'vect__analyzer': (TweetAnalyzer(None, stemmer), 'word'),
        'vect__max_df': (0.5, 0.75, 1.0),
        'vect__max_features': (None, 5000, 10000, 50000),
        'vect__ngram_range': ((1, 1), (1, 2)),  # unigrams or bigrams
        'tfidf__use_idf': (True, False),
        'tfidf__norm': ('l1', 'l2'),
        'clf__fit_intercept': (True, False),
        'clf__n_iter': (5, 10),
        'clf__shuffle': (True, False),
        'clf__loss': ('hinge', 'squared_hinge'),
        'clf__class_weight': ('balanced', {'positive': 5, 'rest_world': 1})
    }

    grid_search = GridSearchCV(text_clfPos, parameters, n_jobs=-1, verbose=1)

    print("Performing grid search on positive vs world...")
    print("pipeline:", [name for name, _ in text_clfPos.steps])
    print("parameters_posvsworld:")
    pprint(parameters)
    t0 = time()
    grid_search.fit(train_data, train_labels)
    print("done in %0.3fs" % (time() - t0))
    print()

    print("Best score: %0.3f" % grid_search.best_score_)
    print("Best parameters set:")
    best_parameters = grid_search.best_estimator_.get_params()
    for param_name in sorted(parameters.keys()):
        print("\t%s: %r" % (param_name, best_parameters[param_name]))
