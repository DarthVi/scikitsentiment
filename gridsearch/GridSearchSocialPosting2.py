from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.ensemble import VotingClassifier
from sklearn.cross_validation import KFold
from customclassifiers.EmojiNaiveClassifier import EmojiClassifier
from customclassifiers.EmojiCustomClassifier import EmojiCustomClassifier
from sklearn.grid_search import GridSearchCV
from nltk.corpus import stopwords
from socialpostanalysis.extractutils import getDataWithLabel
from EvalFileBuilder import Builder
from pprint import pprint
import numpy as np
import nltk
from sklearn import svm
from time import time
from customclassifiers.Doc2VecUtils import Doc2VecModel, Doc2VecTransformer
from customclassifiers.CustomAnalyzer import TweetAnalyzer

if __name__ == "__main__":

    spwords = ['è', 'e', 'il', 'di', 'a', 'da', 'dello', 'del', 'un']

    stemmer_nostopword = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=True)
    stemmer_yesstopword = nltk.stem.snowball.ItalianStemmer(ignore_stopwords=False)

    t0 = time()
    train_data, train_labels = getDataWithLabel("Positivi.txt", "positive", "utf-8")
    train_dataNeg, train_labelsNeg = getDataWithLabel("Negativi.txt", "negative", "utf-8")

    train_data += train_dataNeg
    train_labels += train_labelsNeg

    train_data = np.asarray(train_data)
    train_labels = np.asarray(train_labels)

    text_clfPos = Pipeline([('vect', CountVectorizer()),
                            ('tfidf', TfidfTransformer()),
                            ('clf', svm.LinearSVC()),
                            ])
    emo_clfPos = EmojiCustomClassifier()

    parameters = {
        'vect__analyzer': (TweetAnalyzer(None, stemmer_nostopword), TweetAnalyzer(None, stemmer_yesstopword),
                           TweetAnalyzer('italian', stemmer_nostopword), TweetAnalyzer('italian', None),
                           TweetAnalyzer(),
                           'word'),
        'vect__max_df': (0.5, 0.75, 1.0),
        'vect__max_features': (None, 5000, 10000, 50000),
        'vect__ngram_range': ((1, 1), (1, 2)),  # unigrams or bigrams
        'tfidf__use_idf': (True, False),
        'tfidf__norm': ('l1', 'l2'),
        'clf__dual': (True, False),
        # 'clf__penalty': ('l1', 'l2'),
        'clf__fit_intercept': (True, False),
        # 'clf__loss': ('hinge', 'squared_hinge'),
        'clf__max_iter': (1000, 5000),
        'clf__class_weight': ('balanced', {'positive': 5, 'negative': 1})
    }

    grid_search = GridSearchCV(text_clfPos, parameters, n_jobs=-1, verbose=1, cv=3)

    print("Performing grid search...")
    print("pipeline:", [name for name, _ in text_clfPos.steps])
    print("parameters:")
    pprint(parameters)
    t0 = time()
    grid_search.fit(train_data, train_labels)
    print("done in %0.3fs" % (time() - t0))
    print()

    print("Best score: %0.3f" % grid_search.best_score_)
    print("Best parameters set:")
    best_parameters = grid_search.best_estimator_.get_params()
    for param_name in sorted(parameters.keys()):
        print("\t%s: %r" % (param_name, best_parameters[param_name]))