from TreeTagger import TreeTagger


class TweetAnalyzer:

    """
    Custom analyzer da usare in CountVectorizer
    """

    def __init__(self, stopwords=None, stemmer=None):
        self.tagger = TreeTagger(stopwords, stemmer)

    def __call__(self, doc):
        return self.tagger.tag_text(doc)

    def analyze(self, doc):
        return self.tagger.tag_text(doc)

    def __str__(self):

        stopwords = "stopwords_list" if self.tagger.stopwords is not None else "None"
        stemmer = self.tagger.stemmer if self.tagger.stemmer is not None else "None"

        return "TwitterAnalyzer(" + stopwords.__str__() + "," + stemmer.__str__() + ")"

    def __repr__(self):

        stopwords = "stopwords_list" if self.tagger.stopwords is not None else "None"
        stemmer = self.tagger.stemmer if self.tagger.stemmer is not None else "None"

        return "TwitterAnalyzer(" + stopwords.__repr__() + "," + stemmer.__repr__() + ")"