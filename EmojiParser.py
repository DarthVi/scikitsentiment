import re


class EmojiParser:

    """
    Classe utilizzata per gestire le emoji
    """

    #dizionario di emoticon a cui sono associati degli identificativi di categoria
    emodict = {':-)': '#emoSMILE',
               ':)': '#emoSMILE',
               ':))': '#emoSMILE',
               ':)))': '#emoSMILE',
               ':(': '#emoSAD',
               ':\'(': '#emoSAD',
               ':\')': '#emoSMILE',
               ':\'))': '#emoSMILE',
               ':((': '#emoSAD',
               ':D': '#emoSMILE',
               'D:': '#emoSAD',
               ':O': '#emoSURPRISED',
               ':o': '#emoSURPRISED',
               'o_o': '#emoSURPRISED',
               'O_O': '#emoSURPRISED',
               ';)': '#emoWINK',
               ';-)': '#emoWINK',
               'XD': '#emoLAUGH',
               'xD': '#emoLAUGH',
               ':P': '#emoTONGUE',
               ':p': '#emoTONGUE',
               '^_^': '#emoSMILE',
               '^-^': '#emoSMILE',
               ':|': '#emoPOKERFACE',
               '<3': '#emoHEART',
               '♥': '#emoHEART',
               '❤': '#emoHEART',
               ':\\': '#emoDUBIOUS',
               ':\\\\': '#emoDUBIOUS'}

    def parseLine(self, str):

        """
        :param str: stringa in cui rilevare le emoticon e sostituirle con l'identificativo di categoria
        :return: stringa in cui le emoticon sono state sostituite con i rispettivi identificativi di categoria
        """

        #stringa temporanea su cui operare nel loop e poi da restituire
        ns = str

        for emo in self.emodict.keys():
            pattern = re.compile(re.escape(emo))
            ns = re.sub(pattern, self.emodict[emo], ns)

        return ns